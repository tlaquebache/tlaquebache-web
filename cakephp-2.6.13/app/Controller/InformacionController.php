<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class InformacionController extends AppController {
	public $uses= array('Estado', 'Auditor', 'Colonia', 'Poste');

	public function getEstates(){
		$this->autoRender=false;

		$estados=$this->Estado->getAll();

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('Problema al cargar los estados.');

		if($estados){
			$this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('estados', $estados);
			$this->JarabeResponse->toast('Estados cargados correctamente');
		}

		return $this->JarabeResponse->send();
	}

	public function getColonies(){
		$this->autoRender=false;

		$colonias=$this->Colonia->getAll();

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('Problema al cargar las colonias.');

		if($colonias){
			$this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('colonias', $colonias);
			$this->JarabeResponse->toast('Estados cargados correctamente');
		}

		return $this->JarabeResponse->send();
	}

	public function addColony() {
		$this->autoRender=false;

		$nombre = $this->request->data['nombre'];

	   	$data = $this->Colonia->saveColony($nombre);

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast("Error al guardar la colonia");
		if($data){
			$this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('data', $data);
			$this->JarabeResponse->toast("Colonia guardado correctamente");
		}
		return $this->JarabeResponse->send();
	}

	public function getUserData(){
		$userData=$this->Session->read('Auth');
		$user = $userData;

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('Problema al cargar datos del Usuario.');

		if($userData){
			$this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('user', $user);
			$this->JarabeResponse->toast('Datos cargados correctamente');
		}

		return $this->JarabeResponse->send();
	}

	public function getAuditors () {
	$this->autoRender=false;

	$auditors = $this->Auditor->getAll();

	$this->JarabeResponse->estado(0);
	$this->JarabeResponse->toast('Problema al cargar información.');

	if($auditors){
		$this->JarabeResponse->estado(1);
		$this->JarabeResponse->datos('auditors', $auditors);
		$this->JarabeResponse->toast('Información cargada correctamente');
	}

	return $this->JarabeResponse->send();
	}

	public function getPosteByWeek() {
	$this->autoRender=false;

	$auditor_id = $this->request->data['auditor_id'];
   	$project_id = $this->request->data['project_id'];
   	$week_start = $this->request->data['week_start'];
   	$week_end = $this->request->data['week_end'];

	$posts = $this->Poste->getByWeek($auditor_id, $week_start, $week_end);

	$this->JarabeResponse->estado(0);
	$this->JarabeResponse->toast('Problema al cargar información.');

	if($posts){
		$this->JarabeResponse->estado(1);
		$this->JarabeResponse->datos('posts', $posts);
		$this->JarabeResponse->toast('Información cargada correctamente');
	}

	return $this->JarabeResponse->send();
	}
}
