appMain.controller('ReportsController',['$scope', '$state', '$stateParams', 'Info', 'Edit', 'Create', '$http', function($scope, $state, $stateParams, Info, Edit, Create, $http){

 	$scope.calle_principal = "";
 	$scope.cruce_uno = "";
 	$scope.cruce_dos = "";
 	$scope.colonia = "";
 	$scope.tipo_suelo = "";
 	$scope.descripcion = "";

 	// Inicializar el dropdown con la razón del rechazo del reporte
	$scope.initDropdownSuelo = function () {
    $('.ui.dropdown.suelo').dropdown({
      direction: 'downward'
    });
		$('#dropdownSuelo').dropdown({
			onChange: function (val, text ,b) {
				$scope.tipo_suelo=val;
			}
	  });
	}

	// El dropdown con las razones de rechazo aparece dinamicamente cuando se le da click
	// en rechazar al reporte

 	$scope.init = function () {
	 	Info.data.getUserData().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.ciudadano =  response.datos.user.Ciudadano;
          $('.ui.active.inverted.dimmer').removeClass('active');
				}

		}, function(response) {
      $('.ui.active.inverted.dimmer').removeClass('active');
			console.log(response);
	});
	}

  $scope.saveReporte = function () {
  	 	if ( $scope.calle_principal != "" && $scope.cruce_uno != "" &&
  		$scope.cruce_dos != "" && $scope.colonia != "" &&
  		$scope.tipo_suelo != "") {

  		$('.ui.inverted.dimmer').addClass('active');

  		Create.data.reporte({
        ciudadano_id: $scope.ciudadano.id,
        calle_principal : $scope.calle_principal,
  			cruce_uno : $scope.cruce_uno,
  			cruce_dos : $scope.cruce_dos,
  			colonia : $scope.colonia,
  			tipo_suelo : $scope.tipo_suelo,
  			descripcion : $scope.descripcion
  		})
  		.$promise.then(
  			function(response) {
  				if(response.estado == 1){
  					$('.ui.form').form('clear');
  					Materialize.toast(response.toast,_timeToast);
  				}
          $('.ui.inverted.dimmer').removeClass('active');
  			},
  			function(response) {
  				console.log(response);
  				$('.ui.inverted.dimmer').removeClass('active');
  			}
  		);
  	} else {
  		Materialize.toast("Por favor complete todos los campos",_timeToast);
  	}
  }

}]);
