appMain.controller('AdminsController',['$scope', '$state','Access' , 'Info' , 'Create', '$http', function($scope, $state, Access, Info , Create, $http){
	$scope.seleccionados = [];
	 $scope.tapNum = 0;
	 $scope.projectName="";
	 $scope.projectState = "";

	$scope.name = "";
	$scope.email = "";
	$scope.phone = "";
	$scope.address = "";
	$scope.age = "";
	$scope.emergContact = "";
	$scope.emergPhone =  "";
	$scope.instaladores = null;
	$scope.cuadrillas = [];

	// $scope.emergPhone =  "";

	$scope.init = function() {
  		$scope.addProject =  0;
  		$scope.addAuditor =  0;
			$scope.addInstaller = 0;
			$scope.addCuadrilla = 0;
	}

	// $scope.initDropdown = function() {
	// 	$('.ui.dropdown').dropdown();
	// 	$('#dropdownInstalador').dropdown({
	// 		onChange: function(val, text, b){
	// 		$scope.instaladores = val;
	// 		}
	// 	});
	// }
	//
	// $scope.modalInventario = function(index) {
	// 	$scope.cuadrilla_id = $scope.cuadrillas[index].Cuadrilla.id;
	// 	Info.data.getMaterialCuadrilla({cuadrilla_id: $scope.cuadrilla_id}).$promise.then(
	// 		function(response) {
	// 			if(response.estado == 0) {
	// 					// Materialize.toast(response.toast,_timeToast);
	// 					$scope.materialCuadrillas = 0;
	// 				} else {
	// 					$scope.materialCuadrillas =  response.datos.materialCuadrilla;
	// 				}
	//
	// 		}, function(response) {
	// 			console.log(response);
	// 	});
	// 	Info.data.getAparatoCuadrilla({cuadrilla_id: $scope.cuadrilla_id}).$promise.then(
	// 		function(response) {
	// 			if(response.estado == 0) {
	// 					// Materialize.toast(response.toast,_timeToast);
	// 					$scope.aparatoCuadrillas = 0;
	// 				} else {
	// 					$scope.aparatoCuadrillas =  response.datos.aparatoCuadrilla;
	// 				}
	//
	// 		}, function(response) {
	// 			console.log(response);
	// 	});
	// 	$('.ui.modal.inventario')
	// 	.modal({inverted: true})
	// 	.modal('show')
	// 	;
	// }

	Info.data.getUserData().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.users =  response.datos.user;
					if(response.datos.user.User.type == 1){
						$scope.userName = response.datos.user.Supervisor.nombre + " " + response.datos.user.Supervisor.apellido;
						$scope.navBar = "eight";
					}
					if(response.datos.user.User.type == 2){
						$scope.userName = response.datos.user.Obrero.nombre + " " + response.datos.user.Obrero.apellido;
						$scope.navBar = "five";
					}
					if(response.datos.user.User.type == 3){
						$scope.userName = response.datos.user.Ciudadano.nombre + " " + response.datos.user.Ciudadano.apellido;
						$scope.navBar = "five";
					}
				}

		}, function(response) {
			console.log(response);
	});
	//
	// Info.data.getProjects().$promise.then(
	// 	function(response) {
	// 		if(response.estado == 0) {
	// 				Materialize.toast(response.toast,_timeToast);
	// 			} else {
	// 				$scope.projects = response.datos.projects;
	// 			}
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	//
	// 	}, function(response) {
	// 		console.log(response);
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	// });
	//
	// Info.data.getCuadrillas().$promise.then(
	// 	function(response) {
	// 		if(response.estado == 0) {
	// 			} else {
	// 				$scope.cuadrillas = response.datos.cuadrillas;
	// 			}
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	//
	// 	}, function(response) {
	// 		console.log(response);
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	// });
	//
	// Info.data.getAuditors().$promise.then(
	// 	function(response) {
	// 		if(response.estado == 0) {
	// 				Materialize.toast(response.toast,_timeToast);
	// 			} else {
	// 				$scope.auditors = response.datos.auditors;
	// 			}
	// 			$('.ui.active.inverted.dimmer').removeClass('active');
	//
	// 	}, function(response) {
	// 		console.log(response);
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	// });
	//
	// Info.data.getInstallers().$promise.then(
	// 	function(response) {
	// 		if(response.estado == 0) {
	// 				Materialize.toast(response.toast,_timeToast);
	// 			} else {
	// 				$scope.installers = response.datos.installers;
	// 			}
	// 			$('.ui.active.inverted.dimmer').removeClass('active');
	//
	// 	}, function(response) {
	// 		console.log(response);
	// 		$('.ui.active.inverted.dimmer').removeClass('active');
	// });
	//
	// Info.data.getStates().$promise.then(
	// 	function(response){
	// 		$scope.states=response.datos.estados;
	//     	},
	// 	function(response){
	// 		console.log(response);
	// 	}
	// );
	//
	$('.ui.main.dropdown ') .dropdown();
	// // Agregar un proyecto nuevo
	//
	// $scope.saveProject = function () {
	// 		var colonias = $scope.seleccionados;
	// 		if($scope.projectName != "" && $scope.tapNum != 0 && $scope.projectState != ""){
	// 			$('.ui.inverted.dimmer').addClass('active');
	// 			Create.data.project({nombre : $scope.projectName, estado_id : $scope.projectState, colonias : colonias, num_taps : $scope.tapNum})
	// 					.$promise.then(
	// 						function(response) {
	//
	// 							if (response.toast == "Proyecto guardado correctamente")	{
	// 								$scope.projects.push(response.datos.data);
	// 								$scope.addProject =  0;
	//
	// 								$('.ui.form').form('clear');
	//
	// 							}
	// 							Materialize.toast(response.toast,_timeToast);
	//
	// 							$('.ui.active.inverted.dimmer').removeClass('active');
	// 						},
	// 						function(response) {
	// 							console.log(response);
	// 							$('.ui.active.inverted.dimmer').removeClass('active');
	// 						}
	// 					);
	// 		} else {
	// 				Materialize.toast('Faltan datos' ,_timeToast);
	// 		}
	// }
	//
	// $scope.saveCuadrilla = function() {
	// 		var instaladores = $scope.instaladores;
	// 		console.log(instaladores);
	// 		if($scope.id_megacable && $scope.id_interno && instaladores != null){
	// 			$('.ui.inverted.dimmer').addClass('active');
	// 			Create.data.cuadrilla({id_interno: $scope.id_interno, id_megacable: $scope.id_megacable, instaladores : instaladores})
	// 					.$promise.then(
	// 						function(response) {
	// 							if (response.toast == "Cuadrilla guardada correctamente")	{
	// 								$scope.cuadrillas.push(response.datos.data);
	// 								$scope.addCuadrilla =  0;
	// 								$('.ui.form').form('clear');
	// 								Info.data.getCuadrillas().$promise.then(
	// 									function(response) {
	// 										if(response.estado == 0) {
	// 											} else {
	// 												$scope.cuadrillas = response.datos.cuadrillas;
	// 											}
	// 									}, function(response) {
	// 										console.log(response);
	// 								});
	// 							}
	// 							Materialize.toast(response.toast,_timeToast);
	// 							$('.ui.form').form('clear');
	// 							$('.ui.active.inverted.dimmer').removeClass('active');
	// 						},
	// 						function(response) {
	// 							console.log(response);
	// 							$('.ui.form').form('clear');
	// 							$('.ui.active.inverted.dimmer').removeClass('active');
	// 						}
	// 					);
	// 		} else {
	// 				Materialize.toast('Faltan datos' ,_timeToast);
	// 		}
	// }
	//
	// $scope.saveAuditor = function () {
	//
	// 	if($scope.auto_right == '' || $scope.auto_right == false || $scope.auto_right == undefined) {
	// 			$scope.auto_right = 0;
	// 	} else {
	// 		$scope.auto_right = 1;
	// 	}
	// 	if ( $scope.name != "" && $scope.email != "" &&
	// 		$scope.phone != "" && $scope.address != "" &&
	// 		$scope.auditorState != "" && $scope.age != "" &&
	// 		$scope.emergContact != "" && $scope.emergPhone != "" ) {
	//
	// 		$('.ui.inverted.dimmer').addClass('active');
	//
	// 		Create.data.auditor(
	// 		{	name : $scope.name,
	// 			email : $scope.email,
	// 			phone : $scope.phone,
	// 			address : $scope.address,
	// 			auditorState : $scope.auditorState,
	// 			age : $scope.age,
	// 			emergContact : $scope.emergContact,
	// 			emergPhone : $scope.emergPhone,
	// 			auto_right : $scope.auto_right
	// 		})
	// 		.$promise.then(
	// 			function(response) {
	// 				if(response.toast == "Auditor guardado correctamente"){
	// 					$scope.auditors.push(response.datos.data);
	// 					$scope.addAuditor =  0;
	// 					$('.ui.form').form('clear');
	//
	// 					Materialize.toast(response.toast,_timeToast);
	// 				}
	//
	// 				$('.ui.active.inverted.dimmer').removeClass('active');
	// 			},
	// 			function(response) {
	// 				console.log(response);
	// 				$('.ui.active.inverted.dimmer').removeClass('active');
	// 			}
	// 		);
	// 	} else {
	// 		Materialize.toast("Faltan datos",_timeToast);
	// 	}
	// }
	//
	// $scope.saveInstaller = function () {
	//
	// 	if($scope.auto_right == '' || $scope.auto_right == false || $scope.auto_right == undefined) {
	// 			$scope.auto_right = 0;
	// 	} else {
	// 		$scope.auto_right = 1;
	// 	}
	// 	if ( $scope.name != "" && $scope.email != "" &&
	// 		$scope.phone != "" && $scope.address != "" &&
	// 		$scope.installerState != "" && $scope.age != "" &&
	// 		$scope.emergContact != "" && $scope.emergPhone != "" ) {
	//
	// 		$('.ui.inverted.dimmer').addClass('active');
	//
	// 		Create.data.installer(
	// 		{	name : $scope.name,
	// 			email : $scope.email,
	// 			phone : $scope.phone,
	// 			address : $scope.address,
	// 			auditorState : $scope.installerState,
	// 			age : $scope.age,
	// 			emergContact : $scope.emergContact,
	// 			emergPhone : $scope.emergPhone,
	// 			// auto_right : $scope.auto_right
	// 		})
	// 		.$promise.then(
	// 			function(response) {
	// 				if(response.toast == "Instalador guardado correctamente"){
	// 					$scope.installers.push(response.datos.data);
	// 					$scope.addInstaller =  0;
	// 					$('.ui.form').form('clear');
	//
	// 					Materialize.toast(response.toast,_timeToast);
	// 				}
	//
	// 				$('.ui.active.inverted.dimmer').removeClass('active');
	// 			},
	// 			function(response) {
	// 				console.log(response);
	// 				$('.ui.active.inverted.dimmer').removeClass('active');
	// 			}
	// 		);
	// 	} else {
	// 		Materialize.toast("Faltan datos",_timeToast);
	// 	}
	// }

	$scope.logout = function () {
		Access.User.logout().$promise.then(
			function(response) {
				if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					window.location.replace(_baseurl);
				}
			},
			function(response) {
				console.log(response);
			}
		);
	}

	// Validar que no se repita el estado

	// $scope.validarAuditores = function(index) {
	// 	var valid = $scope.auditors.findIndex(function(item){
	// 		return item.Estado.id === $scope.states[index].Estado.id;});
	//
	// 	if(valid == -1)
	// 		return false;
	// 	else
	// 		return true;
	// }
	//
	// $scope.validarInstaladores = function(index) {
	// 	var valid = $scope.installers.findIndex(function(itemI){
	// 		return itemI.Installer.estado_id === $scope.states[index].Estado.id;});
	//
	// 	if(valid == -1)
	// 		return false;
	// 	else
	// 		return true;
	// }
	//
	// $scope.cancel = function() {
	// 	 $('.ui.form').form('clear');
	// 	 $('.ui.modal').modal('hide');
	// }


}]);
