appMain
	.config(function ($stateProvider, $urlRouterProvider){

	    $stateProvider
			.state ('main', {
				url: '/',
			})
			.state('project', {
				url: '/proyectos',
				templateUrl : 'views/project.html'
			})
			.state('oneProject', {
				url: '/proyecto/:id',
				templateUrl : 'views/oneProject.html'
			})
			.state('projectStatistics', {
				url: '/estadisticas/:id',
				templateUrl : 'views/projectStatistics.html'
			})
			.state('oneAuditor', {
				url: '/Auditor/:id',
				templateUrl : 'views/oneAuditor.html'
			})
			.state('oneInstaller', {
				url: '/Installer/:id',
				templateUrl : 'views/oneInstaller.html'
			})
			.state ('auditor', {
				url: '/auditor',
				templateUrl : 'views/auditor.html'
			})
			.state ('instalador', {
				url: '/instalador',
				templateUrl : 'views/instalador.html'
			})
			.state ('cuadrillas', {
				url: '/cuadrillas',
				templateUrl : 'views/cuadrillas.html'
			})
			.state ('almacen', {
				url: '/almacen',
				templateUrl : 'views/almacen.html'
			})
			.state ('reports', {
				url: '/reportes',
				templateUrl : 'views/reports.html'
			})
			.state ('mgcReport', {
				url: '/reportes/megacable',
				templateUrl : 'views/mgcReport.html'
			})
			.state ('nomina', {
				url: '/nomina',
				templateUrl : 'views/nomina.html'
			})
			.state ('mapa', {
				url: '/mapa/:id',
				templateUrl : 'views/mapa.html'
			})
			.state ('map', {
				url: '/map',
				templateUrl : 'views/mapa.html'
			})
			.state ('autos', {
				url: '/autos',
				templateUrl : 'views/autos.html'
			});

            $urlRouterProvider.otherwise("/");
	});
