<?php
/**
 * Application model for CakePHP.
 *
 * This file is Application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class User extends AppModel {
	public $hasOne = array('Ciudadano', 'Obrero', 'Supervisor');


	public function login($user, $pass)
	{
		$this->recursive=-1;
		$contain=array('Ciudadano', 'Obrero', 'Supervisor');

		$data=$this->find('first', array('conditions'=>array('username'=>$user), 'contain'=>$contain));

		if(!$data){
			return false;
		}

		if(sha1($pass)==$data['User']['password']){
			unset($data['User']['password']);
			return $data;
		}

		return false;
	}

	public function newPass($id, $pass)
	{
		//$data=$this->save(array('id'=>$id, 'password'=> password_hash($pass, PASSWORD_BCRYPT, array('cost'=>12)), 'status'=>1));
		$user = $this->find('first', array('conditions'=>array('User.id'=>$id)));

		$data=$this->save(array('id'=>$id, 'password'=> sha1($pass), 'status'=>1));
		if(!$data){
			return false;
		}
		return $data;
	}
	public function getToken($id)
	{
		$data=$this->find('first', array('conditions'=>array('User.id'=>$id)));
		return $data['User']['token'];
	}

	public function banUser($id)
	{
		return $this->save(array('id'=>$id, 'baneado'=>1));
	}
	public function createUser($data){
		$this->create();
		return $this->save($data);
	}
	public function checkEmail($email){
		$data= $this->find('first', array('conditions'=>array('User.username' => $email)));
		if($data){
			return true;
		}
		return false;
	}
	public function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
    	return substr( str_shuffle( $chars ), 0, $length );
	}
}
