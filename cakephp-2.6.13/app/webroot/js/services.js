appMain.factory('Info', ['$resource', function($resource){
	return {
		data: $resource('/', {}, {

			getStates: {	//	GENERAL - obtiene los estados de un país determinado
				method: 'GET',
				data: {},
				isArray: false,
				url: _baseurl + 'Informacion/getEstates'
			},
			getColonies: {
				method: 'GET',
				data: {},
				isArray: false,
				url: _baseurl + 'Informacion/getColonies'
			},
			getUserData: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Informacion/getUserData'
			},
			getAuditors: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/getAuditors'
			},
			getObreros: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Obreros/getObreros'
			},
			getObrerosAvailable: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Obreros/getObrerosAvailable'
			},
			getProjects: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Projects/getProjects'
			},
			getAuditor: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/getAuditor'
			},
			getInstaller: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Installers/getInstaller'
			},
			getProject: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Projects/getProject'
			},
			getProjectInfo: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Projects/getProjectInfo'
			},
			getAutos: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/getAutos'
			},
			getAutosByState: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/getAutosByState'
			},
			getAuditorsByState: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/getAuditorsByState'
			},
			getReports: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/getReports'
			},
			getCuadrillas: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/getCuadrillas'
			},
			getMgcReports: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/getMgcReports'
			},
			getNomina: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/getNomina'
			},
			getCatalogo: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/getCatalogo'
			},
			getPostesLocationByProject: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Projects/getPostesLocationByProject'
			},
			getMaterials: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/getMaterials'
			},
			getMaterialCuadrilla: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/getMaterialCuadrilla'
			},
			getAparatos: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/getAparatos'
			},
			getAparatoCuadrilla: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/getAparatoCuadrilla'
			},
			getInvAparato: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/getInvAparato'
			},
			addColony: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Informacion/addColony'
			}
		})
	};
}])
.factory('Create', ['$resource', function($resource){
	return {
		data: $resource('/', {}, {

			reporte: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reportes/saveReporte'
			},
			cuadrilla: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/saveCuadrilla'
			},
			auditor: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/saveAuditor'
			},
			installer: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Installers/saveInstaller'
			},
			material: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/saveMaterial'
			},
			aparato: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/saveAparato'
			},
			auto: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/saveAuto'
			}
		})
	};
}])
.factory('Edit', ['$resource', function($resource){
	return {
		data: $resource('/', {}, {

			project: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Projects/editProject'
			},
			addAuditorProject: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/addAuditorProject'
			},
			removeAuditorProject: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/removeAuditorProject'
			},
			auditor: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Auditors/editAuditor'
			},
			installer: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Installers/editInstaller'
			},
			assignAutoAuditor: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/assignAutoAuditor'
			},
			assignAutoInstalador: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/assignAutoInstalador'
			},
			removeAutoAuditor: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/removeAutoAuditor'
			},
			removeAutoInstalador: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/removeAutoInstalador'
			},
			nomina: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/editNomina'
			},
			addMaterial: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/agregarMaterial'
			},
			addAparato: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Almacen/agregarAparato'
			},
			asignarMaterial: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/asignarMaterial'
			},
			asignarAparato: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Cuadrillas/asignarAparato'
			},
			auto: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Autos/editAuto'
			},
			rejectReport: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/rejectReport'
			},
			acceptReport: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Reports/acceptReport'
			}
		})
	};
}])
.factory('Access', ['$resource', function($resource){
	return {
		User: $resource('/', {}, {

			login: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Access/login'
			},
			logout: {
				method: 'POST',
				data: {},
				isArray: false,
				url: _baseurl + 'Access/logout'
			}
		})
	};
}]);
