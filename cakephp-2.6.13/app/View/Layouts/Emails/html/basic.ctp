<style type="text/css">
	.bodyE{
		background-color: #FAFAFA;
	}
	.greenPorter.header{
		color: #AEBD19 !important;
	}
	.contentDiv{
		background-color: #d4d4d5;
		
	}
	
</style>
<link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/Semantic-Org/Semantic-UI-CSS/master/semantic.min.css">
<div class="bodyE ui container">
	<h2 class="ui centered huge grey header">Envios Porter</h2>
	<div class="ui divider"></div>
	<?php echo $this->fetch('content'); ?>
</div>