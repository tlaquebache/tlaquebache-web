<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no" name="viewport" />
    <meta content="Semantic-UI-Forest, collection of design, themes and templates for Semantic-UI." name="description" />
    <meta content="Semantic-UI, Theme, Design, Template" name="keywords" />
    <meta content="PPType" name="author" />
    <meta content="#ffffff" name="theme-color" />
    <title>Tlaquebache.</title>
    <link href="../../static/dist/semantic-ui/semantic.min.css" rel="stylesheet" type="text/css" />
    <link href="../../static/stylesheets/default.css" rel="stylesheet" type="text/css" />
    <link href="../../static/stylesheets/pandoc-code-highlight.css" rel="stylesheet" type="text/css" />
    <script src="../../static/dist/jquery/jquery.min.js"></script>
  </head>
  <body>
    <div class="ui large top fixed hidden menu">
      <div class="ui container">
        <a class="active item">Inicio</a>
        <div class="right menu">
          <div class="item">
            <button class="ui button" (click)="modalLogin()">Log in</button>
          </div>
          <div class="item">
            <a class="ui primary button">Registrate</a>
          </div>
        </div>
      </div>
    </div>
<!--Modal login-->
    <div class="ui modal" ng-controller="AccessController">
      <i class="close icon"></i>
      <div class="header">
        <a class="item" href="">
            <img class="ui tiny image" src="img/logo.png">
        </a>
      </div>
      <div class="content">
        <form class="ui form">
          <div class="field">
            <label>Nombre de usuario</label>
            <input ng-model="username" placeholder="Usuario" type="text" required>
          </div>
          <div class="field">
            <label>Contraseña</label>
            <input ng-model="password"  placeholder="Contraseña" type="password" required>
          </div>
        </form>
      </div>
      <div class="actions">
        <div class="ui blue button" data-ng-click="login()">Iniciar sesión</div>
      </div>
    </div>
    <!--Sidebar Menu-->
    <div class="ui vertical inverted sidebar menu">
      <a class="active item">Inicio</a>
      <a class="item" href="../../webroot/views/login.html">Login</a>
      <a class="item">Registrate</a>
    </div>
    <!--Page Contents-->
    <div class="pusher" ng-controller="AccessController">
      <div class="ui inverted vertical masthead center aligned segment">
        <div class="ui container">
          <div class="ui large secondary inverted pointing menu">
            <a class="toc item"><i class="sidebar icon"></i></a><a class="active item">Inicio</a>
            <div class="right item">
              <a class="ui inverted button" data-ng-click="modalLogin()">Log in</a><button class="ui inverted button">Regístrate</button>
            </div>
          </div>
        </div>
        <div class="ui text container">
          <h1 class="ui inverted header">
            Tlaquebache
          </h1>
          <h2>
            Reporta. Entérate.
          </h2>
          <div class="ui huge primary button">
           Hacer reporte<i class="right arrow icon"></i>
          </div>
        </div>
      </div>
      <div class="ui vertical stripe segment">
        <div class="ui middle aligned stackable grid container">
          <div class="row">
            <div class="eight wide column">
              <h3 class="ui header">
                Haz tu reporte.
              </h3>
              <p>
                Adjúntanos imágenes del bache!
              </p>
              <h3 class="ui header">
                Entérate del estado de tu reporte.
              </h3>
              <p>
                Así es, ahora podrás enterarte el estado de tu reporte y saber también cuando este sea reparado!
              </p>
            </div>
            <div class="six wide right floated column">
              <img class="ui large bordered rounded image" src="img/t6.jpeg" />
            </div>
          </div>
          <div class="row">
            <div class="center aligned column">
              <a class="ui huge button">Galería</a>
            </div>
          </div>
        </div>
      </div>
      <div class="ui vertical stripe quote segment">
        <div class="ui equal width stackable internally celled grid">
          <div class="center aligned row">
            <div class="column">
              <h3>
                "Ahora nuestra calle no tiene baches"
              </h3>
              <p>
                Alicia Perez
              </p>
            </div>
            <div class="column">
              <h3>
                "Hacer un reporte ahora es más sencillo"
              </h3>
              <p>
                Juan Orozco
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="ui vertical stripe segment">
        <div class="ui text container">
          <h3 class="ui header">
            Conoce Tlaquepaque
          </h3>
          <p>
            Jardín Hidalgo.- Lugar que da vida al corazón de San Pedro Tlaquepaque con su hermosa plaza y su característico kiosco rodeado entre frescas palmas, fresnos y rosas que invitan a relajarte y disfrutar de un delicioso antojito mexicano, una nieve raspada y presenciar diferentes actividades cívicas, religiosas y culturales, por lo que se considera un punto ideal de descanso y entretenimiento para nuestros visitantes.
          </p>
          <a class="ui large button">Leer más</a>
        </div>
      </div>
      <div class="ui inverted vertical footer segment">
        <div class="ui container">
          <div class="ui stackable inverted divided equal height stackable grid">
            <div class="three wide column">
              <h4 class="ui inverted header">
                About
              </h4>
              <div class="ui inverted link list">
                <a class="item" href="homepage.html#"> Contáctanos</a>
                <a class="item" href="homepage.html#"> Sobre nosotros</a>

              </div>
            </div>
            <div class="three wide column">
              <h4 class="ui inverted header">
                Servicios
              </h4>
              <div class="ui inverted link list">
                <a class="item" href="homepage.html#"> Reportes</a><a class="item" href="homepage.html#">FAQ</a><a class="item" href="homepage.html#">Tutorial</a><a class="item" href="homepage.html#">Visitanos en Facebook</a>
              </div>
            </div>
            <div class="seven wide column">
              <h4 class="ui inverted header">
              H. Ayuntamiento de San Pedro Tlaquepaque
              </h4>
              <p>

                Independencia #58 Centro San Pedro
                Tlaquepaque
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="../../static/dist/semantic-ui/semantic.min.js"></script>
    <style type="text/css">
      .hidden.menu {
        display: none;
      }

      .masthead.segment {
        min-height: 600px;
        padding: 1em 0em;
        background-color: #ea7f14 !important;
      }
      .masthead .logo.item img {
        margin-right: 1em;
      }
      .masthead .ui.menu .ui.button {
        margin-left: 0.5em;

      }
      .masthead h1.ui.header {
        margin-top: 2em;
        margin-bottom: 0em;
        font-size: 4em;
        font-weight: normal;
      }
      .masthead h2 {
        font-size: 1.7em;
        font-weight: normal;
      }
      .ui.secondary.inverted.pointing.menu {
        border-width: 2px;
        border-color: transparent;
      }

      .ui.vertical.stripe {
        padding: 8em 0em;
      }
      .ui.vertical.stripe h3 {
        font-size: 2em;
      }
      .ui.vertical.stripe .button + h3,
      .ui.vertical.stripe p + h3 {
        margin-top: 3em;
      }
      .ui.vertical.stripe .floated.image {
        clear: both;
      }
      .ui.vertical.stripe p {
        font-size: 1.33em;
      }
      .ui.vertical.stripe .horizontal.divider {
        margin: 3em 0em;
      }

      .quote.stripe.segment {
        padding: 0em;
      }
      .quote.stripe.segment .grid .column {
        padding-top: 5em;
        padding-bottom: 5em;

      }

      .footer.segment {
        padding: 5%;
        background-color: #1b9289 !important;
        width: 100%;
      }

      .secondary.pointing.menu .toc.item {
        display: none;
      }

      @media only screen and (max-width: 100%) {
        .ui.fixed.menu {
          display: none !important;
        }
        .secondary.pointing.menu .item,
        .secondary.pointing.menu .menu {
          display: none;

        }
        .ui.secondary.inverted.pointing.menu {
          border-width: 2px;
          border-color: transparent;
          padding-left: 5%;
        }
        .secondary.pointing.menu .toc.item {
          display: block;

        }

        .masthead.segment {
          min-height: 350px;
        }
        .masthead h1.ui.header {
          font-size: 2em;
          margin-top: 1.5em;
        }
        .masthead h2 {
          margin-top: 0.5em;
          font-size: 1.5em;
        }
      }
    </style>
    <script>
      $(document)
        .ready(function() {
          // fix menu when passed
          $('.masthead')
            .visibility({
              once: false,
              onBottomPassed: function() {
                $('.fixed.menu').transition('fade in');
              },
              onBottomPassedReverse: function() {
                $('.fixed.menu').transition('fade out');
              }
            })
          ;

          // create sidebar and attach to menu open
          $('.ui.sidebar')
            .sidebar('attach events', '.toc.item')
          ;
        })
      ;
    </script>
  </body>
</html>
