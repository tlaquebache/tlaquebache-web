	appMain.controller('ProjectController',['$scope', '$state', 'Info', 'Edit', 'Create', '$stateParams', '$http', function($scope, $state, Info, Edit, Create, $stateParams, $http){
	$scope.project = [];
	$scope.project_auditors = [];
	$scope.projectColony = [];
	$scope.seleccionados = [];
	$scope.auditors = [];

	$scope.init = function() {
  		$scope.edit =  0;
  		$scope.editList =  0;
	}
		
	Info.data.getProject({id : $stateParams.id}).$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);

				} else {					
					$scope.project = response.datos.project;
					$scope.project_auditors = response.datos.project.Auditor;

					$scope.projectName = $scope.project.Project.nombre;
					$scope.projectState = $scope.project.Estado.id;

					$scope.project.ProjectColonia.forEach(function(colonia) {
					     $scope.projectColony.push(colonia.Colonia);
					     $scope.seleccionados.push(colonia.Colonia);
					});  
					$scope.tapNum = parseInt($scope.project.Project.num_taps);4

					// Trae los auditores que pertenecen al estado del proyecto para
					// poder ser asignados al mismo

					Info.data.getAuditorsByState({estado_id :$scope.projectState})
					.$promise.then(
						function(response) {
							if(response.estado == 1) 
								$scope.auditors = response.datos.auditors;
							if($scope.auditors != null)
							$('.ui.active.inverted.dimmer').removeClass('active');

						}, function(response) {
							console.log(response);
							$('.ui.active.inverted.dimmer').removeClass('active');
					}); 
				} 

		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

	$('.ui.dropdown ') .dropdown();
	$scope.assignAuditor = function(auditor, index) {
		$('.ui.inverted.dimmer').addClass('active');
		// Asignar el auditor al proyecto
		
		Edit.data.addAuditorProject({project_id : $scope.project.Project.id, auditor_id : auditor.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);

				// Actualizar la vista
				if(response.estado == 1) {
					$scope.auditors[index].Auditor.project_id=$scope.project.Project.id;
					$scope.project_auditors.push(response.datos.data.Auditor);
				}
				$('.ui.active.inverted.dimmer').removeClass('active');
			},
			function(response) {
				console.log(response);
				$('.ui.active.inverted.dimmer').removeClass('active');
			}
		);
	}

	// Remover un auditor del proyecto
	$scope.removeAuditor = function(auditor, index) {
		$('.ui.inverted.dimmer').addClass('active');

		Edit.data.removeAuditorProject({project_id : $scope.project.Project.id, auditor_id : auditor.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);

				if(response.estado == 1) {
					$scope.auditors[index].Auditor.project_id = null;
					$scope.project_auditors.splice(index, 1);
				}
				$('.ui.active.inverted.dimmer').removeClass('active');
			},
			function(response) {
				console.log(response);
				$('.ui.active.inverted.dimmer').removeClass('active');
			}
		);
	}

	// Editar la información de un proyecto

	$scope.editProject = function(){
  		$('.ui.inverted.dimmer').addClass('active');

  		if( $scope.seleccionados.length == 0)
  			var colonias = $scope.projectColony;
  		else 
  			var colonias = $scope.seleccionados;
  		
		Edit.data.project({
			id : $scope.project.Project.id, 
			nombre : $scope.projectName, 
			estado_id : $scope.projectState, 
			colonias : colonias, 
			num_taps : $scope.tapNum})
			.$promise.then(
				function(response) {
					if (response.toast == "Proyecto editado correctamente")	{	
						$scope.project = response.datos.data;
						$scope.edit = 0;
					}
					
					Materialize.toast(response.toast,_timeToast);
					$('.ui.active.inverted.dimmer').removeClass('active');
				}, 
				function(response) {
					console.log(response);
					$('.ui.active.inverted.dimmer').removeClass('active');
				}
		);
	}

}]);