<html  data-ng-app="appMain">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/semantic.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/materialize-toast-nocolors.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="js/materialize.js"></script>
        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/angular/angular.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-resource.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-sanitize.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-animate.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-cookies.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-ui-router.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-touch.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/semantic.min.css" rel="stylesheet"/>

        <!-- Controller -->
        <script src="js/app.js"></script>
        <script src="js/config.js"></script>
        <script src="js/services.js"></script>
        <script src="js/constantes.js"></script>
        <script src="js/controllers/estados.js"></script>
        <script src="js/controllers/colonias.js"></script>
        <script src="js/controllers/access.js"></script>
        <script src="js/controllers/admin.js"></script>
        <script src="js/controllers/sup.js"></script>
        <script src="js/controllers/project.js"></script>
        <script src="js/controllers/auditor.js"></script>
        <script src="js/controllers/installer.js"></script>
        <script src="js/controllers/autos.js"></script>
        <script src="js/controllers/statistic.js"></script>
        <script src="js/controllers/reports.js"></script>
        <script src="js/controllers/nomina.js"></script>
        <script src="js/controllers/mapa.js"></script>
        <script src="js/controllers/almacen.js"></script>
        <script src="js/controllers/jsPaginacion.js"></script>
        <script src="js/directives.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyDu7q1z_Yne6Uy_2zJwxYtBVzo6VJOiTmQ"></script>

        <title>TlaqueBache</title>

    </head>

    <body id="main">
        <div data-ng-controller="AdminsController" ng-init="init()">
            <div class="ui huge pointing {{navBar}} item menu borderless">
                <div class="ui container" id="container">
                <a class="item" href="">
                    <img class="ui tiny image" src="img/logo.png">
                </a>
                <a class="item" ui-sref = "main">
                    <p class="menu header" >Inicio </p>
                </a>
                <a class="item"  ng-show="users.User.type == 1">
                    <p class="menu header"> Obreros </p>
                </a>
                <a class="item" ui-sref = "cuadrillas" ng-show="users.User.type == 1">
                    <p class="menu header"> Cuadrillas </p>
                </a>
                <a class="item"  ng-show="users.User.type == 1">
                    <p class="menu header"> Reportes </p>
                </a>
                <a class="item"  ng-show="users.User.type == 1">
                    <p class="menu header"> Asignar </p>
                </a>
                <a class="item"  ng-show="users.User.type == 1">
                    <p class="menu header"> Validar </p>
                </a>
                <a class="item"  ng-show="users.User.type == 2">
                    <p class="menu header"> Actualizar </p>
                </a>
                <a class="item"  ng-show="users.User.type == 2">
                    <p class="menu header"> Lista de Trabajo </p>
                </a>
<<<<<<< HEAD
                <a class="item"  ng-show="users.User.type == 3">
                    <p class="menu header"> Reportar </p>
=======
                <a class="item" ui-sref="reports" ng-show="users.User.type == 3">
                    <h3 class="menu header"> Reportar </h3>
>>>>>>> master
                </a>
                <a class="item"  ng-show="users.User.type == 3">
                    <p class="menu header"> Seguimiento </p>
                </a>
                <div class="item">
                    <div class="ui main fluid dropdown item">
                        <span class="text" style="color:#767676" align="right"> {{userName}} </span>
                        <i class="dropdown icon" style="color:#eeeaea"></i>
                        <div class="menu">
                          <!-- <div class="item"><h5 style="color:#767676">Ver Perfil</h5></div> -->
                          <div ng-click="logout()" class="item"><h5 style="color:#bbb">Salir</h5></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="ui container">

                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
   </body>
</html>
