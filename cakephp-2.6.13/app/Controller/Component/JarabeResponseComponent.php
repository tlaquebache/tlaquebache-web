<?php 

/**
 * Este componente se usa para responder peticiones que requieran usar JSON por ejemplo una función 
 * que es llamada a partir de ajax o un servicio en Angular, que puede estar esperando respuesta a una 
 * petición en formato JSON.
 *
 * Debe ser usado para confirmar que la operación fue exitosa.
 */

class JarabeResponseComponent extends Component {

/**
 * A copy of the calling controller
 */
    public $Controller;

/**
 * The data to be set
 * @var array
 */
    protected $Content = array();

/**
 * Default message to be set
 * @var string
 */
    protected $defaultMessage = "No fue inicializado.";

/**
 * Default state to be set
 * @var integer
 */
    protected $defaultState = 0;

/**
 * Runs when the component is instanced
 * @param  Controller $Controller The calling controller
 */
    public function initialize(Controller $Controller) {
        $this->Controller = $Controller;
        $this->Content['toast'] = $this->defaultMessage;
        $this->Content['estado'] = $this->defaultState;
        $this->Content['datos'] = array();
    }

/**
 * Sets the message to be sent
 * @param  string $toast Message
 * @throws CakeException When the needed data is not passed
 */
    public function toast($toast = false) {
        if (!$toast) {
            throw new CakeException('Toast not passed');
        }

        $this->Content['toast'] = $toast;
    }

/**
 * Sets the state to be sent
 * @param  int $estado State
 * @throws CakeException When the needed data is not passed
 */
    public function estado($estado = false) {
        if ($estado === false) {
            throw new CakeException('Estado not passed');
        }

        $this->Content['estado'] = $estado;
    }

/**
 * Sets the data to be sent with a given key
 * @param  strin  $id   Identifying key for the data
 * @param  mixed  $dato The data to be passed
 * @throws CakeException When the needed data is not passed
 */
    public function datos($id = false, $dato = false) {
        if (!$dato) {
            throw new CakeException('Dato not passed');
        }

        if (!$id) {
            throw new CakeException('Id not passed');
        }

        $this->Content['datos'][$id] = $dato;
    }

/**
 * Prepares a response with the given data
 * @param  mixed   $content The data to be sent
 * @param  string  $message Message to be sent
 * @param  int     $codigo  The status code of the response
 */
    public function create($datos, $toast = false, $codigo = false) {
        if (!isset($datos)) {
            $datos = false;
        }

        if (!$codigo) {
            $codigo = $this->defaultState;
        } 

        if (!$toast) {
            $toast = $this->defaultMessage;
        }

        $this->Content = array(
            'datos' => $datos,
            'toast' => $toast,
            'codigo' => $codigo,
        );
    }

/**
 * Returns the set data
 * @param  boolean       $json If set to false it will return an array
 * @return string|array        The set data
 */
    public function send($json = true) {
        if (!$json) {
            return $this->Content;
        }

        return json_encode($this->Content);
    }

    public function BaseErrorResponse($toast = false, $estado = false, $datos = false, $json = true) {
        if (!$toast) {
            $toast = "Ha ocurrido un error interno.";
        }

        if ($estado === false) {
            $estado = 1;
        }

        $this->Content = array(
            'toast' => $toast,
            'datos' => $datos,
            'estado' => $estado,
        );

        if (!$json) {
            return $this->Content;
        }

        return json_encode($this->Content);
    }

    public function NotFoundResponse($data = false) {
        return $this->BaseErrorResponse("El recurso no fue encontrado.", 1);
    }

    public function ForbiddenResponse($data = false) {
        return $this->BaseErrorResponse("Prohibido.", 1);
    }

    public function InsufficientDataResponse($data = false) {
        return $this->BaseErrorResponse("Datos insuficientes.", 1);
    }
}