<?php
/**
* Application level Controller
*
* This file is application-wide controller file. You can put all
* application-wide controller-related methods here.
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

/**
* Application Controller
*
* Add your application-wide methods in the class below, your controllers
* will inherit them.
*
* @package    app.Controller
* @link    http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
*/
class ObrerosController extends AppController {
 public $uses= array('Obrero');
 public $components = array('Mail');

 public function getObrerosAvailable () {
   $this->autoRender=false;

   $obrerosDisponibles = $this->Obrero->getAvailable();

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast('Todos los obreros han sido asignados');

   if($obrerosDisponibles){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('obrerosDisponibles', $obrerosDisponibles);
     $this->JarabeResponse->toast('Información cargada correctamente');
   }

   return $this->JarabeResponse->send();
 }

 public function getObreros () {
   $this->autoRender=false;

   $obreros = $this->Obrero->getAll();

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast('Problema al cargar información.');

   if($obreros){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('obreros', $obreros);
     $this->JarabeResponse->toast('Información cargada correctamente');
   }

   return $this->JarabeResponse->send();
 }

 public function getInstaller () {
  $this->autoRender=false;

  $id = $this->request->data['id'];

  $installerData = $this->Installer->getOne($id);

  $this->JarabeResponse->estado(0);
  $this->JarabeResponse->toast('Problema al cargar los datos.');

  if($installerData){
    $installer=array(
      "installer"=>$installerData
    );
  } else {
      $installer = $installerData;

      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->datos('installer', $installer);
      $this->JarabeResponse->toast('Instalador cargado correctamente');
    }

  return $this->JarabeResponse->send();
  }


 public function saveInstaller(){
   $this->autoRender=false;

   $nombre_completo = $this->request->data['name'];
   $correo = $this->request->data['email'];
   $telefono = $this->request->data['phone'];
   $direccion = $this->request->data['address'];
   $estado_id = $this->request->data['auditorState'];
   $edad = $this->request->data['age'];
   $contacto_emergencia = $this->request->data['emergContact'];
   $tel_emergencia = $this->request->data['emergPhone'];
   $derecho_auto = $this->request->data['auto_right'];
   $password = $this->User->rand_passwd();
   $code_password = sha1($password);

   $data = "";

   $user_data = array(
     'username' => $correo,
     'password' => $code_password,
     'email' => $correo,
     'type' => 3 );

   $validate_installer = $this->Installer->validateInstaller($correo);

   if($validate_installer == null) {
     $user_data = $this->User->createUser($user_data);
     $user_id = $user_data['User']['id'];
     $tipo_usuario = "instalador";

     $installerData = $this->Installer->saveInstaller($user_id, $nombre_completo, $correo, $telefono, $direccion, $edad, $contacto_emergencia, $tel_emergencia, $estado_id, $derecho_auto);

     $installer_id = $installerData['Installer']['id'];
     $data =  $this->Installer->getOne($installer_id);

     $this->JarabeResponse->toast("Error al guardar el instalador");
  }

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast("El correo ya fue registrado anteriormente. ");

   if($data){
    $email_status = $this->Mail->send_mail($correo, $nombre_completo, $password, $tipo_usuario);

    if($email_status) {
      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->datos('data', $data);
      $this->JarabeResponse->toast("Instalador guardado correctamente");
    }
   }

   return $this->JarabeResponse->send();
 }

   public function saveToken () {
   $this->autoRender=false;

   $id = $this->request->data['id'];
   $token = $this->request->data['token'];

   $userToken = $this->UserToken->saveToken($id, $token);

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast('Problema al guardar el Token.');

   if($userToken){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('userToken', $userToken);
     $this->JarabeResponse->toast('Token guardado correctamente');
   }

   return $this->JarabeResponse->send();
  }
}
