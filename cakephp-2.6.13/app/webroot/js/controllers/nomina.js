appMain.controller('NominasController',['$scope', '$state', '$stateParams', 'Info', 'Edit', '$http', function($scope, $state, $stateParams, Info, Edit, $http){

	$scope.indexX = 0;
	$scope.indexY = 0;
	$scope.myIndexX = -1;
	$scope.myIndexY = -1;

	// Traer los datos de nómina y hacer los calculos para las columnas calculadas

	$scope.init = function () {
	 	Info.data.getNomina().$promise.then(
			function(response) {
				if(response.estado = 0) {
					$scope.nominas = [];
					return Materialize.toast(response.toast,_timeToast);
				} else
					$scope.nominas = response.datos.nomina;
					$scope.nominas.forEach(function(nomina) {
						for( var el in nomina.Nomina ) {
							nomina.Nomina[el] = Number(nomina.Nomina[el]);
				  		}
							Info.data.getCatalogo({
								user_type : nomina.Report.Auditor.User.type
							}).$promise.then(
								(data)=>{
									nomina.Nomina.perceptions = Number(data.datos.nomina.Catalogo.percepciones * nomina.Nomina.totalTaps);
									nomina.Nomina.impuesto = Number((data.datos.nomina.Catalogo.impuesto * nomina.Nomina.perceptions) / 100);
									nomina.Nomina.reserva = Number(data.datos.nomina.Catalogo.reserva * nomina.Nomina.totalTaps);
								},
								(error)=>{});
						nomina.deducciones
						= nomina.Nomina.impuesto
						+ nomina.Nomina.reserva
						+ nomina.Nomina.anticipos
						+ nomina.Nomina.prestamo
						+ nomina.Nomina.abono
						+ nomina.Nomina.imss
						+ nomina.Nomina.gas
						+ nomina.Nomina.gasolina;

						nomina.subtotal
						= nomina.Nomina.perceptions
						- nomina.deducciones
						+ nomina.Nomina.bono
						+ nomina.Nomina.apoyo;

						nomina.total = nomina.subtotal - nomina.Nomina.infonavit;

					});
				$('.ui.active.inverted.dimmer').removeClass('active');
			}, function(response) {
				console.log(response);
				$('.ui.active.inverted.dimmer').removeClass('active');
		});
	}

	// Se busca el catálogo correspondiente al tipo de usuario

	// $scope.getCatalogo = function () {
	// 	 	Info.data.getCatalogo({
	// 			user_type : nomina.Report.Auditor.User.type
	// 		}).$promise.then(
	// 			(data)=>{
	// 				nomina.Nomina.impuesto = nomina.Catalogo.impuesto;
	// 			},
	// 			(error)=>{});
	// 			console.log(user_type)
	// }
	// El input aparece en la celda a la que se le da click

	$scope.showInput = function (x, y) {
		document.querySelector('input').autofocus = true;
		$scope.myIndexX = x;
		$scope.myIndexY = y;
	}

	// Cada que un input se desenfoca se guarda el valor
	$scope.saveField = function (field, index, value) {
		console.log(field + " " + index + " " + value);
		console.log($scope.nominas);

		Edit.data.nomina({
			nomina_id : $scope.nominas[index].Nomina.id,
			field : field,
			value : value
		}).$promise.then(
			function(response) {
				if(response.estado == 0)
					Materialize.toast(response.toast,_timeToast);
			},
			function(response) {
				console.log(response);
		});

		// Se hacen los calculos de las columnas calculadas

		$scope.nominas[index].deducciones
		= $scope.nominas[index].Nomina.impuesto
		+ $scope.nominas[index].Nomina.reserva
		+ $scope.nominas[index].Nomina.anticipos
		+ $scope.nominas[index].Nomina.prestamo
		+ $scope.nominas[index].Nomina.abono
		+ $scope.nominas[index].Nomina.imss
		+ $scope.nominas[index].Nomina.gas
		+ $scope.nominas[index].Nomina.gasolina;

		$scope.nominas[index].subtotal
		= $scope.nominas[index].Nomina.perceptions
		- $scope.nominas[index].deducciones
		+ $scope.nominas[index].Nomina.bono
		+ $scope.nominas[index].Nomina.apoyo;

		$scope.nominas[index].total = $scope.nominas[index].subtotal - $scope.nominas[index].Nomina.infonavit;
	}



}]);
