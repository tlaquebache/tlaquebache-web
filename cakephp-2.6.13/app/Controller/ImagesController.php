<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class ImagesController extends AppController {
   	public $uses = array('Image');

   public function saveImage() {
		$this->autoRender=false;

		$image = $_FILES['file'];
		$dir=$this->request->data["dir"];
		$poste_id=$this->request->data["poste_id"];
		$dirAbsoluta= 'img/'.$dir;

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('Problema al guardar la imagen.');

		list($nom, $ext)=split("[.]", $image['name']);
		
		if (!file_exists( $dirAbsoluta)) {
			if (!mkdir($dirAbsoluta, 0777, true)) {
				return $this->JarabeResponse->send();
			}
		}
		
	    $filename = md5(date('YmdHis'));
	    $filename = explode(" ", $filename);
	    $filename = implode("", $filename);
		$filename = $filename.'.'.$ext;
		
	    $file = $dirAbsoluta."/".$filename;
				   
		if (move_uploaded_file($image['tmp_name'], $file)) {


			$saved = $this->Image->saveOne($poste_id, $filename);

			if($saved)	{
				$this->JarabeResponse->estado(1);
				$this->JarabeResponse->datos('image', $saved);
				$this->JarabeResponse->toast('Imagen guardada correctamente');
			}
		}
		return $this->JarabeResponse->send();
	}
}