<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AccessController extends AppController {

  public $layout = "access";
	public $uses= array('User');

  public function beforeFilter(){
    if($this->request->action=="logout"){
      $this->logout();
    }
    if(!is_null($this->Session->read('Auth.User'))){
      $this->layout = 'main';
      $this->redirect(array('controller' => 'AppView', 'action' => 'index'));
    }
    parent::beforeFilter();
  }

   	public function index()
   	{
   		$this->autoRender=true;
      $this->layout='access';
    }

   public function login()
   {
      $this->autoRender=false;

	   	$user=$this->request->data['username'];
	   	$pass=$this->request->data['password'];
	   	$data=$this->User->login($user, $pass);

      $this->JarabeResponse->estado(0);
      $this->JarabeResponse->toast("Correo o contraseña incorrectos");
      if($data){
        $this->JarabeResponse->estado(1);
        $this->JarabeResponse->toast("login");
        $this->JarabeResponse->datos('user', $data);
        if($data['User']['type']!==null){
          $this->Session->write('Auth', $data);
        }
      }
   		return $this->JarabeResponse->send();
   	}
    public function setPass()
   {
      $this->autoRender=false;

      $id=$this->request->data['id'];
      $pass=$this->request->data['password'];

      $data=$this->User->newPass($id, $pass);

      $this->JarabeResponse->estado(0);
      $this->JarabeResponse->toast("Correo o contraseña incorrectos");
      if($data){
        $this->JarabeResponse->estado(1);
        $this->JarabeResponse->toast("Contraseña cambiada correctamente");

      }
      return $this->JarabeResponse->send();
    }

    public function logout(){
      $this->autoRender=false;
      $this->Session->delete('Auth');
      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->toast("logout");
      return $this->JarabeResponse->send();
    }
}
