appMain.controller('MapController',['$scope','$state', 'Info','$http','$filter',"$stateParams", function($scope, $state, Info, $http, $filter,$stateParams){
	$scope.centro={lat: 20.6739383, lng: -103.4054535};
	$scope.colores=[
	'http://maps.google.com/mapfiles/ms/icons/red-dot.png',//rojo
	'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',//amarillo
	'http://maps.google.com/mapfiles/ms/icons/green-dot.png' //verde
	];
	$scope.project_id = $stateParams.id;


	$scope.map;
	$scope.marcadores=[];
	$scope.getLocations=function(id){
		Info.data.getPostesLocationByProject({"id":id}).$promise.then(
			function(response){			
				$scope.postes=response.datos.postes;
				$scope.putMarkers();
			},
			function(response){
				console.log(response);
			}
		);
	}
	$scope.getProjectInfo=function(id){
		Info.data.getProjectInfo({"id":id}).$promise.then(
			function(response){			
				$scope.project=response.datos.project;
				$scope.putMarkers();
			},
			function(response){
				console.log(response);
			}
		);
	}($scope.project_id); //mandamos a llamar la funcion para traer proyecto inmediatamente despuesd e declararla...



	$scope.putMarkers=function(){
		let lat=0,long=0;
	  for (var i = 0; i < $scope.postes.length; ++i) {
	 			
	 			lat+= parseFloat($scope.postes[i].Poste.lat);
	 			long+= parseFloat($scope.postes[i].Poste.long);
	 			
		    var marker = new google.maps.Marker({
	        	id:i,
	        	icon: $scope.colores[0],
			    position: new google.maps.LatLng({lat: parseFloat($scope.postes[i].Poste.lat), lng: parseFloat($scope.postes[i].Poste.long)}),
			    map: $scope.map,			    
			    title: "Poste #"+" "+$scope.postes[i].Poste.id+" del proyecto"
			  });
		    	var content=`
		    	<div class="ui card">
					Poste #${$scope.postes[i].Poste.id} del proyecto
		    	</div>`;
		    setText(marker, content);

		    $scope.marcadores.push(marker);	  		
	  	
	  }
	  	$scope.centro =  new google.maps.LatLng({lat: parseFloat(lat/$scope.postes.length), lng: parseFloat(long/$scope.postes.length)});
	 				$scope.map.setCenter ($scope.centro);
	}
	 			
	$scope.initMap=function() {
		$scope.getLocations($scope.project_id);
 		$scope.map = new google.maps.Map(document.getElementById('map'), {
          center: $scope.centro,
          // scrollwheel: true,
          zoom: 15
          
        });
  	}


		// Attaches an info window to a marker with the provided message. When the
		// marker is clicked, the info window will open with the secret message.
	function setText(marker, content) {
	  var infowindow = new google.maps.InfoWindow({
	    content: content
	  });
	  infowindow.addListener('closeclick',function(){
    		$scope.ventanasAbiertas--;	  
	});
	  marker.addListener('click', function() {
	  	 var map = infowindow.getMap();
    	if(map !== null && typeof map !== "undefined"){
    		$scope.ventanasAbiertas--;   
	    	infowindow.close();    		
    	}
    	else{
    		$scope.ventanasAbiertas++;
	    	infowindow.open(marker.get('map'), marker);
    	}
	  });
	}

	//barra lateral de descripcion
	//
	$scope.contarContratosActivos=function(taps){
		let cont = 0;
		taps.forEach((el,index)=>{
			console.log("contando contratos "+index);
			console.log(el);
			if(el.con_status==1){
				cont++;
			}
		})
		return cont;
	}
$scope.verPoste=function(poste,val){
		$scope.postes.forEach((el,index)=>{
			el.edit=true;
		})
		poste.edit = val;
	 	$scope.map.setCenter ( new google.maps.LatLng({lat: parseFloat(poste.Poste.lat), lng: parseFloat(poste.Poste.long)}));
	 	$scope.map.setZoom( 17);

	 	if(val==true){
	 	$scope.map.setZoom( 13);
	 	$scope.map.setCenter ( $scope.centro);

	 	}
	}



}]);