appMain.controller('AutosController',['$scope', '$state', 'Info', 'Create', 'Edit', '$http', function($scope, $state, Info, Create, Edit, $http){
	$scope.kilometraje = "";
	$scope.placas = "";
	$scope.seguro_id = "";
	$scope.compania = "";
	$scope.compania = "";
	$scope.modalType = 1;
	$scope.autoId = 0;
	$scope.autoState = "";

	$scope.init = function() {
  		$scope.addAuto =  0;
	}

	Info.data.getAutos().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.autos =  response.datos.autos;
				}
			$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	}); 

	$scope.saveAuto = function() {
		if ( $scope.kilometraje != "" && $scope.placas != "" && 
			$scope.projectState != 0 && $scope.seguro_id != "" &&
			$scope.compania ) {
		$('.ui.inverted.dimmer').addClass('active');
		Create.data.auto ( {
			kilometraje : $scope.kilometraje,
			placas : $scope.placas,
			seguro_id : $scope.seguro_id,
			compania : $scope.compania,
			estado_id : $scope.projectState
		})
		.$promise.then(
			function(response) {
				if(response.toast == "Auto guardado correctamente"){
						$scope.autos.push(response.datos.data);
						$scope.addAuto =  0;

						$('.ui.form').form('clear');
						Materialize.toast(response.toast,_timeToast);
					}			
					$('.ui.active.inverted.dimmer').removeClass('active');				
			},
			function(response) {
					console.log(response);
					$('.ui.active.inverted.dimmer').removeClass('active');
			}
		);
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

	$scope.editAuto = function(){
		if ( $scope.kilometraje != "" && $scope.placas != "" && 
			$scope.projectState != 0 && $scope.seguro_id != "" &&
			$scope.compania ) {
			$('.ui.inverted.dimmer').addClass('active');

			Edit.data.auto({	
					id : $scope.autoId, 
					placas : $scope.placas, 
					estado_id : $scope.projectState, 
					kilometraje : $scope.kilometraje, 
					seguro_id : $scope.seguro_id, 
					compania_seguro : $scope.compania})
				.$promise.then(
					function(response) {

						// Actualizar la vista
								
						if (response.toast == "Auto editado correctamente")	{	
							var index = $scope.autos.findIndex(i => i.Auto.id == $scope.autoId);
							$scope.autos[index].Auto.placas = $scope.placas;
							$scope.autos[index].Auto.kilometraje = $scope.kilometraje;
							$scope.autos[index].Auto.seguro_id = $scope.seguro_id;
							$scope.autos[index].Auto.compania_seguro = $scope.compania;
							$scope.autos[index].Estado = response.datos.data.Estado;

							$scope.addAuto =  0;

						}
						
						$('.ui.active.inverted.dimmer').removeClass('active');
						Materialize.toast(response.toast,_timeToast);

					}, 
					function(response) {
						console.log(response);
						$('.ui.active.inverted.dimmer').removeClass('active');
					}
			);
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

	$scope.modalAddAuto = function() {
		$scope.addAuto = 1;
		$scope.modalType = "";
		$scope.kilometraje = "";
		$scope.placas = "";
		$scope.seguro_id = "";
		$scope.compania = "";
		$scope.projectState = "";
		$scope.autoId = "";
	}

	$scope.editAutoModal = function(auto, estado) {
		$scope.addAuto = 1;
		$scope.modalType = 1;
		$scope.kilometraje = auto.kilometraje;
		$scope.placas = auto.placas;
		$scope.seguro_id = auto.seguro_id;
		$scope.compania = auto.compania_seguro;
		$scope.projectState = auto.estado_id;
		$scope.autoId = auto.id;
	}
}]);