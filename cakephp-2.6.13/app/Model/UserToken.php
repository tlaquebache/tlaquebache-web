<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class UserToken extends AppModel {
	public $belongsTo = array('User');

	public function saveToken($user_id, $token) {

		$data = $this->find('first', array('conditions'=>array('UserToken.user_id'=>$user_id, 'UserToken.token'=>$token)));

		if(!$data){
			$this->create();
			$data2 = $this->save(array('user_id'=>$user_id, 'token'=>$token));
			if($data2){
				return 1;
			}
			return 2;
		}
		return 3;
	}

	public function getTokens($id) {
		$this->recursive = -1;

		return $this->find('all', array('conditions'=>array('user_id'=>$id)));
	}
}