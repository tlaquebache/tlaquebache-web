<?php

  class AppViewController extends AppController {
  	
    public $layout = "main";
  	public $result;

    public function beforeFilter() {
        if(is_null($this->Session->read('Auth.User'))) { 
          $this->redirect(array('controller'=>'Access', 'action'=>'index'));
        }

          parent::beforeFilter();
      }


     	public function index() {
      $this->autoRender=true;   
     	}
  }