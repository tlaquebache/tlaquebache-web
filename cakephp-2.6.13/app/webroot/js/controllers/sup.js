appMain.controller('SupsController',['$scope', '$state','Access' , 'Info' , 'Create', '$http', function($scope, $state, Access, Info , Create, $http){
	$scope.seleccionados = [];
	 $scope.tapNum = 0;
	 $scope.projectName="";
	 $scope.projectState = "";

	$scope.name = "";
	$scope.email = "";
	$scope.phone = "";
	$scope.address = "";
	$scope.age = "";
	$scope.emergContact = "";
	$scope.emergPhone =  "";
	$scope.obreros = null;
	$scope.cuadrillas = [];


	$scope.init = function() {
			$scope.addObrero = 0;
			$scope.addCuadrilla = 0;
	}

	$scope.initDropdown = function() {
		$('.ui.dropdown').dropdown();
		$('#dropdownObrero').dropdown({
			onChange: function(val, text, b){
			$scope.obrerosDisponibles = val;
			}
		});
	}


	Info.data.getUserData().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.users =  response.datos.user;
					if(response.datos.user.User.type == 1){
						$scope.userName = response.datos.user.Supervisor.nombre + " " + response.datos.user.Supervisor.apellido;
						$scope.navBar = "eight";
					}
					if(response.datos.user.User.type == 2){
						$scope.userName = response.datos.user.Obrero.nombre + " " + response.datos.user.Obrero.apellido;
						$scope.navBar = "five";
					}
					if(response.datos.user.User.type == 3){
						$scope.userName = response.datos.user.Ciudadano.nombre + " " + response.datos.user.Ciudadano.apellido;
						$scope.navBar = "five";
					}
				}

		}, function(response) {
			console.log(response);
	});

	Info.data.getCuadrillas().$promise.then(
		function(response) {
			if(response.estado == 0) {
				} else {
					$scope.cuadrillas = response.datos.cuadrillas;
				}
			$('.ui.active.inverted.dimmer').removeClass('active');

		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

	Info.data.getObreros().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.obreros = response.datos.obreros;
				}
				$('.ui.active.inverted.dimmer').removeClass('active');

		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

  Info.data.getObrerosAvailable().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.obrerosDisponibles = response.datos.obrerosDisponibles;
				}
				$('.ui.active.inverted.dimmer').removeClass('active');

		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});


	$('.ui.main.dropdown ') .dropdown();

	$scope.saveCuadrilla = function() {
			let obreros = $scope.obrerosDisponibles;
			console.log(obreros);
			if($scope.nombre_cuadrilla && obreros != null){
				$('.ui.inverted.dimmer').addClass('active');
				Create.data.cuadrilla({nombre_cuadrilla: $scope.nombre_cuadrilla, sup_id: $scope.users.Supervisor.id, obreros : obreros})
						.$promise.then(
							function(response) {
								if (response.toast == "Cuadrilla guardada correctamente")	{
									$scope.cuadrillas.push(response.datos.data);
									$scope.addCuadrilla =  0;
									$('.ui.form').form('clear');
									Info.data.getCuadrillas().$promise.then(
										function(response) {
											if(response.estado == 0) {
												} else {
													$scope.cuadrillas = response.datos.cuadrillas;
												}
										}, function(response) {
											console.log(response);
									});
								}
								Materialize.toast(response.toast,_timeToast);
								$('.ui.form').form('clear');
								$('.ui.active.inverted.dimmer').removeClass('active');
							},
							function(response) {
								console.log(response);
								$('.ui.form').form('clear');
								$('.ui.active.inverted.dimmer').removeClass('active');
							}
						);
			} else {
					Materialize.toast('Faltan datos' ,_timeToast);
			}
	}

	$scope.logout = function () {
		Access.User.logout().$promise.then(
			function(response) {
				if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					window.location.replace(_baseurl);
				}
			},
			function(response) {
				console.log(response);
			}
		);
	}



}]);
