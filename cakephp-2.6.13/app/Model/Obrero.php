<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Obrero extends AppModel {
	public $belongsTo = array('User', 'Cuadrilla');

	public function getAll(){
	$this->recursive=-1;
	return $this->find('all', array('order'=>array('Obrero.id ASC')));
	}

	public function getAvailable(){
	$this->recursive=-1;
	$conditions = array('Obrero.cuadrilla_id'=>null);
	return $this->find('all', array('conditions'=>array($conditions), 'order'=>array('Obrero.id ASC')));
	}

	public function saveCuadrilla($id, $cuadrilla_id){

		$this->recursive=-1;

		$data =  $this->find('first', array('conditions'=>array('Obrero.id'=>$id)));
		if($data) {
			$this->id = $id;
			$this->saveField('cuadrilla_id', $cuadrilla_id);
			return true;
		}
		return false;
	}

}
