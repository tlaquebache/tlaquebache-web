appMain.controller('ColoniasController',['$scope','$state', 'Info', function($scope, $state, Info){
	$scope.colonias=[];
	$scope.seleccionados=[];
	$scope.otherColony = "";

	// Agregar una colonia que no estaba en la lista

	$scope.addColony = function() {
		if ($scope.otherColony != "") {
			var not_duplicado=true;

            $scope.colonias.forEach(function(colonia){
                if(colonia.Colonia.nombre == $scope.otherColony)
                    not_duplicado=false;
            });

            // Verificar que la colonia no esté repetida

            if(not_duplicado){
            	Info.data.addColony({nombre : $scope.otherColony})
				.$promise.then(
				function(response) {
					$scope.colonias.push(response.datos.data);
					$scope.otherColony = "";
				}, 
				function(response) {
					console.log(response);
				});
            } else {
            	Materialize.toast('La colonia ya existe' ,_timeToast);
            }
		} else {
			Materialize.toast('Este campo esta vacío' ,_timeToast);
		} 
	}

	if($scope.ident=="" || $scope.ident==undefined)
		$scope.ident="coloniasDirective";

	// Traer las colonias para mostrarlas en el dropdown
	
	Info.data.getColonies().$promise.then(
		function(response){
			$scope.colonias = response.datos.colonias;
			$('#'+$scope.ident).dropdown({
				onChange: function (val, text ,b) {
						$scope.model=parseInt(val);
						var not_duplicado=true;

						if($scope.seleccionados != null) {
							$scope.seleccionados.forEach(function(colonia){
			                if(colonia.id == $scope.model)
			                    not_duplicado=false;
			            	});
						}
						if(not_duplicado) {
							$scope.seleccionados.push({'id' : parseInt(val), 'nombre' : text});
						}
						$scope.$apply();
					}
	    	});
		},
		function(response){
			console.log(response);
		}
	);

	// Remover una colonia seleccionada por el dropdown
	$scope.delete = function(selected) {
		var index = $scope.seleccionados.indexOf(selected);

		if (index > -1) 
		$scope.seleccionados.splice(index, 1);
	}
}]);