appMain.controller('StatisticController',['$scope', '$state', '$stateParams', 'Info', '$http', function($scope, $state, $stateParams, Info, $http){
 	
 	$scope.project = {};
 	$scope.projectColony = [];
 	$scope.auditors_id = [];
 	$scope.auditors = [];
 	$scope.postes = [];
 	$scope.taps = 0;
 	$scope.active = 0;
 	$scope.canceled = 0;
 	$scope.suspended = 0;

 	$scope.init = function () {
	 	Info.data.getProject({id : $stateParams.id}).$promise.then(
			function(response) {
				if(response.estado == 0) {
						Materialize.toast(response.toast,_timeToast);
					} else {					
						$scope.project = response.datos.project;
						$scope.postes = $scope.project.Poste;

						// La tabla se divide en colonias

						$scope.project.ProjectColonia.forEach(function(colonia) {
							if ($scope.projectColony.indexOf(colonia) === -1){
						    	$scope.projectColony.push(colonia.Colonia);
						 	}
						});  

						// Se clasifican los postes por auditor
						$scope.postes.forEach(function(poste) {
							if ($scope.auditors_id.indexOf(poste.auditor_id) === -1){
						    	$scope.auditors_id.push(poste.auditor_id);
						 	}
						 	$scope.taps += parseInt(poste.connectors4);
							$scope.taps += parseInt(poste.connectors8);

							// Se hacen los calculos de totales dependiendo del estado del tap

							poste.Tap.forEach(function(tap) {
								switch(tap.con_status) {
									case '1' : $scope.active ++;
										break;
									case '2' : $scope.canceled ++;
										break;
									case '3' : $scope.suspended ++;
										break;
							 	}
			 				})	
						}); 
						$scope.auditors_id.forEach(function(auditor_id) {
							$scope.auditor_in_project(auditor_id);
						}); 
					} 
				$('.ui.active.inverted.dimmer').removeClass('active');
			}, function(response) {
				console.log(response);
				$('.ui.active.inverted.dimmer').removeClass('active');
		});
	};

	// Selecciona los auditores que tienen postes en ese proyecto
	$scope.auditor_in_project = function(auditor_id){
		var bnd = 0;

		$scope.project.Auditor.forEach(function(auditor) {
			if (auditor_id == auditor.id){
				bnd = 1;
		    	 $scope.auditors.push(auditor);
		 	}
	 	})
	 	if(bnd == 0) {

	 		// Si el auditor tiene taps en ese proyecto pero ahora está asignado a otro
	 		// se trae de la base de datos
		 	Info.data.getAuditor({id : auditor_id}).$promise.then(
			function(response) {
				if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.auditors.push(response.datos.auditor.Auditor);
				}
			}, function(response) {
				console.log(response);
			});
		}		
	}

	// Asignar valores a la tabla
 	$scope.tableData = function(colony, auditor, index) {

 		var colony_id = colony.id;
 		var auditor_id = auditor.id;
 		var temp={};
 		temp.auditor_name = auditor.nombre_completo;
 		temp.colony_name = colony.nombre;
 		$scope.auditor_name = auditor.nombre_completo;
 		temp.conectors_active = 0;
 		temp.conectors_cancel = 0;
 		temp.conectors_suspend = 0;
 		temp.basic_service = 0;
 		temp.con_service = 0;
 		temp.internet_service = 0;
 		temp.tel_ervice = 0;
 		temp.connectors_pirate = 0;
 		temp.connectors_auto_con = 0;
 		temp.connectors_auto_can = 0;
 		temp.filters_num = 0;
 		temp.connectors_filter = 0;
 		temp.connectors_plate = 0;
 		temp.connectors_locks = 0;
 		temp.connectors8 = 0;
 		temp.connectors4 = 0;

 		// Se hacen los calculos por poste

		$scope.postes.forEach(function(poste) {
			if(poste.colonia_id == colony_id && auditor_id == poste.auditor_id){
				poste.Tap.forEach(function(tap) {
					switch(tap.con_status) {
						case '1' : temp.conectors_active ++;
							break;
						case '2' : temp.conectors_cancel ++;
							break;
						case '3' : temp.conectors_suspend ++;
							break;
				 	}
				 	if(tap.basic_service == 1)
				 		temp.basic_service ++;
				 	if(tap.con_service == 1)
				 		temp.con_service ++;
				 	if(tap.internet_service == 1)
				 		temp.internet_service ++;
				 	if(tap.tel_service == 1)
				 		temp.tel_service ++;
		 		})	
				temp.connectors_pirate += parseInt(poste.connectors_pirate);
				temp.connectors_auto_con += parseInt(poste.connectors_auto_con);
				temp.connectors_auto_can += parseInt(poste.connectors_auto_can);
				temp.filters_num += parseInt(poste.filters_num);
				temp.connectors_filter += parseInt(poste.connectors_filter);
				temp.connectors_plate += parseInt(poste.connectors_plate);
				temp.connectors_locks += parseInt(poste.connectors_locks);
				temp.connectors4 += parseInt(poste.connectors4);
				temp.connectors8 += parseInt(poste.connectors8);
			}
		});
		// Se agregan al arreglo de colonias en este proyecto
		$scope.projectColony[index].tapInfo=temp;

		return true;
 	}
 	
}]);