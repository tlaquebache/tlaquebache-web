<html  data-ng-app="appMain">
    <head>

        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="css/semantic.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/materialize-toast-nocolors.css"/>
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="js/materialize.js"></script>
        <script type="text/javascript" src="js/semantic.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/angular/angular.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-resource.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-sanitize.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-animate.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-cookies.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-ui-router.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-touch.min.js"></script>
        <script src="js/app.js"></script>
        <script src="js/services.js"></script>
        <script src="js/constantes.js"></script>
        <script src="js/controllers/access.js"></script>
        <script src="js/controllers/estados.js"></script>
        <script src="js/directives.js"></script>

        <title>tlaqueBache</title>

    </head>

    <body   id="main">
        <!-- <div class="ui huge pointing menu borderless">
            <div class="ui container">
                <a class="item" href="">
                    <img class="ui tiny image" src="img/logo.png">
                </a>
            </div>

        </div> -->
        <div class="ui hidden divider"></div>
            <div class="ui container">
                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>
       </body>
</html>
