<?php
App::uses('CakeEmail', 'Network/Email');
App::uses("Component", "Controller");

class MailComponent extends Component {

    public function send_mail($to, $username, $password, $tipo_usuario) {
        $email = new CakeEmail();
        $email->config('smtp');

        $message = "Bienvenido a Drakon, ". $username ." ahora eres ". $tipo_usuario ." \n\n Usuario: " . $to . "\n\n Contraseña : " . $password;

        $email->from(array("drakon-no-responder@jarabesoft.com" => "Drakon"))
             ->to($to)
             ->subject("Bienvenido a Drakon");

        return $email->send($message);
    }
}
