appMain.controller('AlmacenController',['$scope', '$state', 'Info', 'Edit', '$stateParams', '$http', 'Create', function($scope, $state, Info, Edit, $stateParams, $http, Create){

	// Traer inforación del auditor
	$scope.inventarioAparatos = [];
	// $scope.aparatoInfo = [];
	$scope.prueba = [];

	$scope.init = function() {
    $scope.addMaterial =  0;
    $scope.addAparato = 0;
    $scope.material_id;
		$scope.aparato_id;
    $('.tabular.menu .item').tab();
	}

	$scope.materialDropdown = function() {
		$('.ui.selection.dropdown').dropdown();
		$('#material').dropdown({
			onChange: function(val, text, b){
			return $scope.cuadrilla = val;
			console.log($scope.cuadrilla);
			}
		});
	}

	$scope.aparatoCDropdown = function() {
		$('.ui.selection.dropdown').dropdown();
		$('#aparato').dropdown({
			onChange: function(val, text, b){
			return $scope.cuadrilla = val;
			}
		});
	}

	$scope.aparatoSDropdown = function() {
		$('.iu.selection.dropdown').dropdown();
		$('#serie').dropdown({
			onChange: function(val, text, b){
			return $scope.num_serie = val;
			}
		});
	}

	$scope.getInvAparato = function(index) {
		$scope.aparato_id = $scope.aparatos[index].CatalogoAparato.id;
		Info.data.getInvAparato({aparato_id: $scope.aparato_id}).$promise.then(
		function(response) {
			if(response.estado == 0) {
					//Materialize.toast(response.toast,_timeToast);
					$scope.inventarioAparatos[index] = 0;
				} else {
					$scope.inventarioAparatos[index] =  response.datos.inventarioAparato;
				}
			$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});
}

  Info.data.getMaterials().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.materials =  response.datos.material;
				}
			$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

  Info.data.getAparatos().$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {
					$scope.aparatos =  response.datos.aparato;
				}
			$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

  $scope.saveMaterial = function() {
		if ($scope.nombre != "" && $scope.descripcion != "") {
      if ($scope.cantidad == null || $scope.cantidad == "undefined") {
        $scope.cantidad = 0;
      }
  		$('.ui.inverted.dimmer').addClass('active');
  		Create.data.material({
  			nombre : $scope.nombre,
  			descripcion : $scope.descripcion,
        cantidad : $scope.cantidad
  		})
  		.$promise.then(
  			function(response) {
  				if(response.toast == "Artículo guardado correctamente"){
  						$scope.materials.push(response.datos.data);
              Info.data.getMaterials().$promise.then(
                function(response) {
                  if(response.estado == 0) {
                      Materialize.toast(response.toast,_timeToast);
                    } else {
                      $scope.materials =  response.datos.material;
                    }
                  $('.ui.active.inverted.dimmer').removeClass('active');
                }, function(response) {
                  console.log(response);
                  $('.ui.active.inverted.dimmer').removeClass('active');
              });
              $('.ui.form').form('clear');
  						$scope.addMaterial =  0;
  						Materialize.toast(response.toast,_timeToast);
  					}
  			},
  			function(response) {
  					console.log(response);
  					$('.ui.active.inverted.dimmer').removeClass('active');
  			}
  		);
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

  $scope.saveAparato = function() {
		if ($scope.nombre != "" && $scope.num_serie != "") {
		$('.ui.inverted.dimmer').addClass('active');
		Create.data.aparato({
			nombre : $scope.nombre,
			num_serie : $scope.num_serie
		})
		.$promise.then(
			function(response) {
				if(response.toast == "Aparato guardado correctamente"){
						$scope.aparatos.push(response.datos.data);
            Info.data.getAparatos().$promise.then(
              function(response) {
                if(response.estado == 0) {
                    Materialize.toast(response.toast,_timeToast);
                  } else {
                    $scope.aparatos =  response.datos.aparato;
                  }
                // $('.ui.active.inverted.dimmer').removeClass('active');
              }, function(response) {
                console.log(response);
                $('.ui.active.inverted.dimmer').removeClass('active');
            });
            $('.ui.form').form('clear');
            $scope.addAparato =  0;
						// Materialize.toast(response.toast,_timeToast);
					}
					$('.ui.active.inverted.dimmer').removeClass('active');
					Materialize.toast(response.toast,_timeToast);
			},
			function(response) {
					console.log(response);
			});
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

  $scope.modalAddMaterial = function() {
		$scope.addMaterial = 1;
    $scope.modalType = "";
		$scope.nombre = "";
		$scope.descripción = "";
	}

  $scope.modalAddAparato = function() {
		$scope.addAparato = 1;
    $scope.modalType = "";
		$scope.nombre = "";
		$scope.num_serie = "";
	}

  $scope.mostrarPopup = function(index) {
    $scope.material_id = $scope.materials[index].InventarioMaterial.id;
    $scope.articulo = $scope.materials[index].CatalogoMaterial.nombre;
    $('.ui.modal.material')
    .modal({inverted: true})
    .modal('show')
    ;
  }

	$scope.aparatoPopup = function(index) {
		$scope.aparato_id = $scope.aparatos[index].CatalogoAparato.id;
		$scope.articulo = $scope.aparatos[index].CatalogoAparato.nombre;
		$('.ui.modal.aparato')
		.modal({inverted: true})
		.modal('show')
		;
	}

	$scope.modalMaterial = function(index) {
		$scope.cuadrilla = $scope.materialDropdown();
		$scope.material_id = $scope.materials[index].InventarioMaterial.id;
		$scope.articulo = $scope.materials[index].CatalogoMaterial.nombre;
		$('.ui.modal.asignarMaterial')
		.modal({inverted: true})
		.modal('show')
		;
	}

	$scope.modalAparato = function(index) {
		$scope.cuadrilla = $scope.aparatoCDropdown();
		$scope.num_serie = $scope.aparatoSDropdown();
		$scope.prueba = $scope.aparatos[index];
		$('.ui.modal.asignarAparato')
		.modal({inverted: true})
		.modal('show')
		;
	}

  $scope.agregarMaterial = function(index) {
      if ($scope.cantidad == null || $scope.cantidad == "undefined") {
        $scope.cantidad = 0;
      }
      $('.ui.inverted.dimmer').addClass('active');
      Edit.data.addMaterial({
        material_id: $scope.material_id,
        cantidad : $scope.cantidad
      })
      .$promise.then(
        function(response) {
          if(response.toast == "Artículo actualizado correctamente"){
              $scope.materials.push(response.datos.data);
							Info.data.getMaterials().$promise.then(
								function(response) {
									if(response.estado == 0) {
										} else {
											$scope.materials =  response.datos.material;
										}
									$('.ui.active.inverted.dimmer').removeClass('active');
								}, function(response) {
									console.log(response);
									$('.ui.active.inverted.dimmer').removeClass('active');
							});
							$('.ui.form').form('clear');
							$scope.addMaterial =  0;
            }
						Materialize.toast(response.toast,_timeToast);
						$('.ui.active.inverted.dimmer').removeClass('active');
        },
        function(response) {
            console.log(response);
            $('.ui.active.inverted.dimmer').removeClass('active');
        }
      );
  }

	$scope.agregarAparato = function(index) {
		// $scope.aparato_id = $scope.aparatos[index].InventarioAparato.catalogo_aparato_id;
    if ($scope.num_serie !== null || $scope.num_serie !== "undefined") {
      $('.ui.inverted.dimmer').addClass('active');
      Edit.data.addAparato({
        aparato_id: $scope.aparato_id,
        num_serie : $scope.num_serie
      })
      .$promise.then(
        function(response) {
          if(response.toast == "Artículo actualizado correctamente"){
              $scope.aparatos.push(response.datos.data);
							Info.data.getAparatos().$promise.then(
								function(response) {
									if(response.estado == 0) {
										} else {
											$scope.aparatos =  response.datos.aparato;
										}
									$('.ui.active.inverted.dimmer').removeClass('active');
								}, function(response) {
									console.log(response);
									$('.ui.active.inverted.dimmer').removeClass('active');
							});
							$('.ui.form').form('clear');
							$scope.addAparato =  0;
            }
						Materialize.toast(response.toast,_timeToast);
						$('.ui.active.inverted.dimmer').removeClass('active');
        },
        function(response) {
            console.log(response);
            $('.ui.active.inverted.dimmer').removeClass('active');
        }
      );
		}
  }

	$scope.asignarMaterial = function() {
      if ($scope.cantidad == null || $scope.cantidad == "undefined") {
        $scope.cantidad = 0;
      }
      $('.ui.inverted.dimmer').addClass('active');
      Edit.data.asignarMaterial({
				cuadrilla_id: Number($scope.cuadrilla),
        material_id: Number($scope.material_id),
        cantidad : $scope.cantidad
      })
      .$promise.then(
        function(response) {
          if(response.toast == "Material asignado correctamente"){
              $scope.materials.push(response.datos.data);
							Info.data.getMaterials().$promise.then(
								function(response) {
									if(response.estado == 0) {
										} else {
											$scope.materials =  response.datos.material;
										}
									$('.ui.active.inverted.dimmer').removeClass('active');
								}, function(response) {
									console.log(response);
									$('.ui.active.inverted.dimmer').removeClass('active');
							});
							$('.ui.form').form('clear');
							$scope.addMaterial =  0;
            }
						Materialize.toast(response.toast,_timeToast);
						$('.ui.active.inverted.dimmer').removeClass('active');
        },
        function(response) {
            console.log(response);
            $('.ui.active.inverted.dimmer').removeClass('active');
        }
      );
  }

	$scope.asignarAparato = function() {
		if ($scope.cuadrilla !== null && $scope.cuadrilla !== "undefined" && $scope.num_serie !== null && $scope.num_serie !== "undefined") {
			console.log($scope.num_serie);
			$scope.aparatoInfo = $scope.num_serie.split(',');
			console.log($scope.aparatoInfo);
			$scope.num_serie = $scope.aparatoInfo[0];
			$scope.aparato_id = Number($scope.aparatoInfo[1]);

			$('.ui.inverted.dimmer').addClass('active');
			Edit.data.asignarAparato({
				cuadrilla_id: Number($scope.cuadrilla),
				aparato_id: Number($scope.aparato_id),
				num_serie: $scope.num_serie
			})
			.$promise.then(
				function(response) {
					if(response.toast == "Aparato asignado correctamente"){
							$scope.aparatos.push(response.datos.data);
							Info.data.getAparatos().$promise.then(
								function(response) {
									if(response.estado == 0) {
										} else {
											$scope.aparatos =  response.datos.aparato;
										}
									$('.ui.active.inverted.dimmer').removeClass('active');
								}, function(response) {
									console.log(response);
									$('.ui.active.inverted.dimmer').removeClass('active');
							});
							$('.ui.form').form('clear');
							$scope.addAparato =  0;
						}
						Materialize.toast(response.toast,_timeToast);
						$('.ui.active.inverted.dimmer').removeClass('active');
				},
				function(response) {
						console.log(response);
						$('.ui.active.inverted.dimmer').removeClass('active');
				}
			);
		}
	}

	$scope.cancel = function() {
		$('.ui.form').form('clear');
		$('.ui.modal').modal('hide');
	}

}]);
