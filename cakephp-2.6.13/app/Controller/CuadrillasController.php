<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class CuadrillasController extends AppController {
	public $uses= array('Obrero', 'Cuadrilla');

	public function getCuadrillas() {
		$this->autoRender=false;

		$cuadrillas = $this->Cuadrilla->getAll();

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('No se encontraron cuadrillas.');

		if($cuadrillas){
		$this->JarabeResponse->estado(1);
		$this->JarabeResponse->datos('cuadrillas', $cuadrillas);
		$this->JarabeResponse->toast('Información cargada correctamente');
		}
		return $this->JarabeResponse->send();
	}

	public function saveCuadrilla() {
		$this->autoRender = false;

    $sup_id = $this->request->data['sup_id'];
		$nombre_cuadrilla = $this->request->data['nombre_cuadrilla'];
		$obreros = $this->request->data['obreros'];
		$workers = explode(",", $obreros);

		$cuadrilla = $this->Cuadrilla->saveOne($sup_id, $nombre_cuadrilla);
		$cuadrilla_id = intval($cuadrilla['Cuadrilla']['id']);
		$this->JarabeResponse->toast('Error al guardar');

		if ($cuadrilla_id) {
			foreach ($workers as $worker) {
				$cuadrillasData = $this->Obrero->saveCuadrilla($worker, $cuadrilla_id);
			}
			$this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('data', $cuadrilla);
			$this->JarabeResponse->toast("Cuadrilla guardada correctamente");
		}
		return $this->JarabeResponse->send();
	}

	public function asignarMaterial() {
		$this->autoRender = false;

		$cuadrilla_id = $this->request->data['cuadrilla_id'];
		$material_id = $this->request->data['material_id'];
		$cantidad = $this->request->data['cantidad'];
		$this->JarabeResponse->toast('Error al guardar');

		$cuadrilla = $this->CuadrillaMaterial->getByMaterial($cuadrilla_id, $material_id, $cantidad);
		if(!$cuadrilla) {
			$cuadrilla = $this->CuadrillaMaterial->saveOne($cuadrilla_id, $material_id, $cantidad);
		}

		if($cuadrilla) {
			$data = $this->InventarioMaterial->removeMaterial($material_id, $cantidad);
				$this->JarabeResponse->estado(1);
				$this->JarabeResponse->datos('data', $cuadrilla);
				$this->JarabeResponse->toast("Material asignado correctamente");
			}
			return $this->JarabeResponse->send();
	}

}
