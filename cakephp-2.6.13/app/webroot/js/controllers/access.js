appMain.controller('AccessController',['$scope','$state', 'Access', '$http', function($scope, $state, Access, $http){

	$scope.username="";
	$scope.password="";

	$scope.success=false;
	$scope.tryToRegister=false;

	$scope.modalLogin = function(){
		$('.ui.modal').modal('show');
		console.log(true);
	}

	$scope.checkForm=function(formValid){
		$scope.tryToRegister=true;
		$('.ui.form').addClass('loading');
		if($scope.signupForm.$valid && $scope.checkSpecialFields()){
			$scope.createCompanyUser();
		}
		else{
			$('.ui.form').removeClass('loading');
		}
	}

	$scope.login=function(){
		if($scope.username!="" && $scope.password!=""){
			$('.field').addClass('disabled');
			Access.User.login({username: $scope.username, password: $scope.password}).$promise.then(
				function(response){
					if(response.estado==0){
						Materialize.toast(response.toast,_timeToast);
					}
					else{
						window.location.replace(_baseurl);
					}
					$('.field').removeClass('disabled');
				},
				function(response){
					console.log(response);
				}
			);
		}
	}
	$scope.goToRegister=function(){
		window.location.href=_baseurl+"singup";
	}
	$scope.goToLogin=function(){
		window.location.href=_baseurl+"Auth";
	}
}]);
