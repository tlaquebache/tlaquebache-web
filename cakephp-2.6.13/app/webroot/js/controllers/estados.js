appMain.controller('EstadosController',['$scope','$state', 'Info', function($scope, $state, Info){
	$scope.estados=[];
	$scope.seleccionado="";
	if($scope.ident=="" || $scope.ident==undefined)
		$scope.ident="estadosDirective";
	
	// Traer los datos de los estados para ostrarlos en el dropdown 
	
	Info.data.getStates().$promise.then(
		function(response){
			$scope.estados=response.datos.estados;

			// Adignar el valor de la colonia cada vez que este cambie 

			$('#'+$scope.ident).dropdown({
				onChange: function (val, text ,b) {
						$scope.model=parseInt(val);
						$scope.$apply();				
					}
	    	});
	    	if($scope.model!="")
	    		$('#'+$scope.ident).dropdown('set text', $scope.estados[$scope.model-1].Estado.nombre);
		},
		function(response){
			console.log(response);
		}
	);
}]);