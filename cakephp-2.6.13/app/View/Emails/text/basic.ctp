
<style type="text/css">
    .bodyE{
        background-color: #FAFAFA ;
    }
    .greenPorter.header{
        color: #AEBD19 !important;
    }
    .contentDiv{
        background-color: #d4d4d5 ;
        
    }
    .white{
        
    }
</style>
<link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/Semantic-Org/Semantic-UI-CSS/master/semantic.min.css">
<div class="bodyE ui container" style="background-color: #FAFAFA;display: block;max-width: 100%!important;">
    <h2 class="ui centered huge grey header" style="font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;line-height: 1.28571429em;margin: calc(2rem - .14285714em) 0 1rem;font-weight: 700;padding: 0;font-size: 2em;margin-top: -.14285714em;border: none;text-transform: none;color: #767676!important;min-height: 1em;text-align: center;">Envios Porter</h2>
    <div class="ui divider" style="margin: 1rem 0;line-height: 1;height: 0;font-weight: 700;text-transform: uppercase;letter-spacing: .05em;color: rgba(0,0,0,.85);-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;-webkit-tap-highlight-color: transparent;font-size: 1rem;border-top: 1px solid rgba(34,36,38,.15);border-bottom: 1px solid rgba(255,255,255,.1);"></div>
	<?php echo $this->fetch('content'); ?>
</div>