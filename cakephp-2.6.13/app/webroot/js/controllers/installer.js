appMain.controller('InstallerController',['$scope', '$state', 'Info', 'Edit', '$stateParams', '$http', function($scope, $state, Info, Edit, $stateParams, $http){
	$scope.autoRight ="";
	$scope.project = "No asignado";

	// Traer inforación del auditor

	$scope.init = function() {
  		$scope.edit =  0;
	}

	Info.data.getInstaller({id : $stateParams.id}).$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {

					$scope.installer = response.datos.installer;

					$scope.name = $scope.installer.Installer.nombre_completo;
					$scope.email= $scope.installer.Installer.correo;
					$scope.phone = $scope.installer.Installer.telefono;
					$scope.address = $scope.installer.Installer.direccion;
					$scope.installerState = $scope.installer.Estado.id;
					$scope.age = $scope.installer.Installer.edad;
					$scope.emergContact = $scope.installer.Installer.contacto_emergencia;
					$scope.emergPhone= $scope.installer.Installer.tel_emergencia;
					$scope.auto_right = parseInt($scope.installer.Installer.derecho_auto);

					if ($scope.installer.Installer.derecho_auto == "1"){
						$("#checkbox").prop("checked", true);

						if($scope.installer.Auto.id == null)
							$scope.autoRight = "Si : Sin asignar";
						else
							$scope.autoRight = "Si : " +  $scope.installer.Auto.placas;

						Info.data.getAutosByState({estado_id : $scope.installer.Estado.id}).$promise.then(
							function(response) {
								if(response.estado == 0)
									Materialize.toast(response.toast,_timeToast);
								 else
									$scope.autos = response.datos.autos;

							}, function(response) {
								console.log(response);
						});

					} else if ($scope.installer.Installer.derecho_auto == "0") {
						$scope.autoRight = "No ";

						$("#checkbox").prop("checked", false);
					}

					if($scope.installer.Project.id != null) {
						$scope.project = $scope.installer.Project.nombre;
					}
				 }
				$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

	// Editar la información del auditor
	$scope.editInstaller = function(){
		if(!($("#checkbox").prop('checked'))){
			$scope.auto_right =  0;
		};

		if($scope.auto_right == '' || $scope.auto_right == false || $scope.auto_right == undefined) {
				$scope.auto_right = 0;
		} else {
			$scope.auto_right = 1;
		}
		if ( $scope.name != "" && $scope.email != "" &&
			$scope.phone != "" && $scope.address != "" &&
			$scope.installerState != "" && $scope.age != "" &&
			$scope.emergContact != "" && $scope.emergPhone != "" ) {

	  	$('.ui.inverted.dimmer').addClass('active');

			Edit.data.installer({
				id : $scope.installer.Installer.id,
				nombre_completo : $scope.name,
				correo : $scope.email,
				telefono : $scope.phone ,
				direccion : $scope.address ,
				estado_id : $scope.installerState ,
				edad : $scope.age ,
				contacto_emergencia : $scope.emergContact ,
				tel_emergencia : $scope.emergPhone,
				derecho_auto : $scope.auto_right
			})
			.$promise.then(
				function(response) {

					if (response.estado == 1)	{
						$scope.installer = response.datos.data;
						$scope.edit =  0;
						// Actualizar la forma cuando el auditor sea editado

						if ($scope.installer.Installer.derecho_auto == "1"){
						$("#checkbox").prop("checked", true);

						if( $scope.installer.Auto.id == null)
							$scope.autoRight = "Si : Sin asignar";
						 else
							$scope.autoRight = "Si : " +  $scope.installer.Auto.placas;

						Info.data.getAutosByState({estado_id : $scope.installer.Estado.id}).$promise.then(
							function(response) {
								if(response.estado == 0)
									Materialize.toast(response.toast,_timeToast);
								 else
									$scope.autos = response.datos.autos;
							}, function(response) {
								console.log(response);
						});

					} else if ($scope.installer.Installer.derecho_auto == "0") {
						var index = $scope.autos.indexOf($scope.autos.find(i => i.Auto.id === $scope.installer.Auto.id));

						Edit.data.removeAutoInstalador({auto_id : $scope.installer.Auto.id, installer_id : $scope.installer.Auditor.id})
						.$promise.then(
							function(response) {
								Materialize.toast(response.toast,_timeToast);
								if(response.estado == 1) {
									$("#checkbox").prop("checked", false);
									$scope.autoRight = "No ";
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				};
				Materialize.toast(response.toast,_timeToast);
        //
				$('.ui.active.inverted.dimmer').removeClass('active');
				},
				function(response) {
					console.log(response);
					$('.ui.active.inverted.dimmer').removeClass('active');
				}
			)
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

	// Asignar un auto a un auditor cuando este tenga derecho a auto

	$scope.assignAuto = function(auto, index){
		$('#button1').addClass('loading');
		Edit.data.assignAutoInstalador({auto_id : auto.id, installer_id : $scope.installer.Installer.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);

				if(response.estado == 1) {
					$scope.autos[index].Auto.installer_id = response.datos.data.Auto.installer_id;
					$scope.installer.Auto.id = response.datos.data.Auto.id;
					$scope.autoRight = "Si : " + auto.placas;
				}
				$('#button1').removeClass('loading');
			},
			function(response) {
				console.log(response);
			$('#button1').removeClass('loading');
			}
		);
	}

	// Quitar el auto

	$scope.removeAuto = function(auto_id, index) {
		$('#button2').addClass('loading');
		Edit.data.removeAutoInstalador({auto_id : auto.id, installer_id : $scope.installer.Installer.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);
				if(response.estado == 1) {
					$scope.autos[index].Auto.installer_id = null;
					$scope.installer.Auto.id = null;
					$scope.autoRight = "Si : Sin asignar";
					return true;
				}
				$('#button2').removeClass('loading');
			},
			function(response) {
				console.log(response);
				$('#button2').removeClass('loading');
			}
		);
	}

	$scope.addAutoModal = function(){
		$('#autosModal').modal('show');
	}
	$scope.cancel = function() {
		 $('.ui.modal').modal('hide');
	}

}]);
