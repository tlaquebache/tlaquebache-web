appMain.controller('AuditorController',['$scope', '$state', 'Info', 'Edit', '$stateParams', '$http', function($scope, $state, Info, Edit, $stateParams, $http){
	$scope.autoRight ="";
	$scope.project = "No asignado";

	// Traer inforación del auditor

	$scope.init = function() {
  		$scope.edit =  0;
	}
	
	Info.data.getAuditor({id : $stateParams.id}).$promise.then(
		function(response) {
			if(response.estado == 0) {
					Materialize.toast(response.toast,_timeToast);
				} else {

					$scope.auditor = response.datos.auditor;

					$scope.name = $scope.auditor.Auditor.nombre_completo;
					$scope.email= $scope.auditor.Auditor.correo;
					$scope.phone = $scope.auditor.Auditor.telefono;
					$scope.address = $scope.auditor.Auditor.direccion;
					$scope.auditorState = $scope.auditor.Estado.id;
					$scope.age = $scope.auditor.Auditor.edad;
					$scope.emergContact = $scope.auditor.Auditor.contacto_emergencia;
					$scope.emergPhone= $scope.auditor.Auditor.tel_emergencia;
					$scope.auto_right = parseInt($scope.auditor.Auditor.derecho_auto);

					if ($scope.auditor.Auditor.derecho_auto == "1"){
						$("#checkbox").prop("checked", true);

						if( $scope.auditor.Auto.id == null)
							$scope.autoRight = "Si : Sin asignar";
						else 
							$scope.autoRight = "Si : " +  $scope.auditor.Auto.placas;

						Info.data.getAutosByState({estado_id : $scope.auditor.Estado.id}).$promise.then(
							function(response) {
								if(response.estado == 0) 
									Materialize.toast(response.toast,_timeToast);
								 else 
									$scope.autos = response.datos.autos;
								
							}, function(response) {
								console.log(response);
						});
						 
					} else if ($scope.auditor.Auditor.derecho_auto == "0") {
						$scope.autoRight = "No ";
							
						$("#checkbox").prop("checked", false);
					}
	
					if($scope.auditor.Project.id != null) {
						$scope.project = $scope.auditor.Project.nombre;
					} 
				 }
				$('.ui.active.inverted.dimmer').removeClass('active');
		}, function(response) {
			console.log(response);
			$('.ui.active.inverted.dimmer').removeClass('active');
	});

	// Editar la información del auditor
	$scope.editAuditor = function(){
		if(!($("#checkbox").prop('checked'))){
			$scope.auto_right =  0;
		};

		if($scope.auto_right == '' || $scope.auto_right == false || $scope.auto_right == undefined) {
				$scope.auto_right = 0;
		} else {
			$scope.auto_right = 1;
		}
		if ( $scope.name != "" && $scope.email != "" && 
			$scope.phone != "" && $scope.address != "" &&
			$scope.auditorState != "" && $scope.age != "" &&
			$scope.emergContact != "" && $scope.emergPhone != "" ) {

	  	$('.ui.inverted.dimmer').addClass('active');
	  
			Edit.data.auditor({
				id : $scope.auditor.Auditor.id, 
				nombre_completo : $scope.name,
				correo : $scope.email,
				telefono : $scope.phone ,
				direccion : $scope.address ,
				estado_id : $scope.auditorState ,
				edad : $scope.age ,
				contacto_emergencia : $scope.emergContact ,
				tel_emergencia : $scope.emergPhone,
				derecho_auto : $scope.auto_right
			})
			.$promise.then(
				function(response) {
							
					if (response.estado == 1)	{	
						$scope.auditor = response.datos.data;
						$scope.edit =  0;
						// Actualizar la forma cuando el auditor sea editado

						if ($scope.auditor.Auditor.derecho_auto == "1"){
						$("#checkbox").prop("checked", true);

						if( $scope.auditor.Auto.id == null) 
							$scope.autoRight = "Si : Sin asignar";
						 else 
							$scope.autoRight = "Si : " +  $scope.auditor.Auto.placas;

						Info.data.getAutosByState({estado_id : $scope.auditor.Estado.id}).$promise.then(
							function(response) {
								if(response.estado == 0) 
									Materialize.toast(response.toast,_timeToast);
								 else 
									$scope.autos = response.datos.autos;
							}, function(response) {
								console.log(response);
						});
						 
					} else if ($scope.auditor.Auditor.derecho_auto == "0") {
						var index = $scope.autos.indexOf($scope.autos.find(i => i.Auto.id === $scope.auditor.Auto.id));
						
						Edit.data.removeAutoAuditor({auto_id : $scope.auditor.Auto.id, auditor_id : $scope.auditor.Auditor.id})
						.$promise.then(
							function(response) {
								Materialize.toast(response.toast,_timeToast);
								if(response.estado == 1) { 
									$("#checkbox").prop("checked", false);
									$scope.autoRight = "No ";
								}
							},
							function(response) {
								console.log(response);
							}
						);
					}
				}
				Materialize.toast(response.toast,_timeToast);

				$('.ui.active.inverted.dimmer').removeClass('active');
				}, 
				function(response) {
					console.log(response);
					$('.ui.active.inverted.dimmer').removeClass('active');
				}
			);
		} else {
			Materialize.toast("Faltan datos",_timeToast);
		}
	}

	// Asignar un auto a un auditor cuando este tenga derecho a auto

	$scope.assignAuto = function(auto, index){
		$('#button1').addClass('loading');
		Edit.data.assignAutoAuditor({auto_id : auto.id, auditor_id : $scope.auditor.Auditor.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);

				if(response.estado == 1) {
					$scope.autos[index].Auto.auditor_id = response.datos.data.Auto.auditor_id;
					$scope.auditor.Auto.id = response.datos.data.Auto.id;
					$scope.autoRight = "Si : " + auto.placas;
				}
				$('#button1').removeClass('loading');
			},
			function(response) {
				console.log(response);
			$('#button1').removeClass('loading');
			}
		);
	}

	// Quitar el auto 

	$scope.removeAuto = function(auto_id, index) {
		$('#button2').addClass('loading');
		Edit.data.removeAutoAuditor({auto_id : auto_id, auditor_id : $scope.auditor.Auditor.id})
		.$promise.then(
			function(response) {
				Materialize.toast(response.toast,_timeToast);
				if(response.estado == 1) { 
					$scope.autos[index].Auto.auditor_id = null;
					$scope.auditor.Auto.id = null;
					$scope.autoRight = "Si : Sin asignar";
					return true;	
				}
				$('#button2').removeClass('loading');
			},
			function(response) {
				console.log(response);
				$('#button2').removeClass('loading');
			}
		);
	}

	$scope.addAutoModal = function(){
		$('#autosModal').modal('show');
	}
	$scope.cancel = function() {
		 $('.ui.modal').modal('hide');
	}

}]);