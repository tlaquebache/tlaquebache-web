appMain.directive('jsEstados', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        model: "=estado",
        ident: "@ident"
      },
      controller: "EstadosController",
      templateUrl:"views/directivas/estados.html",
      replace: true
    };
});

appMain.directive('jsColonias', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        model: "=colonia",
        ident: "@ident",
        seleccionados: "=array"
      },
      controller: "ColoniasController",
      templateUrl:"views/directivas/colonias.html",
      replace: true
    };
});

appMain.directive('jsPaginacion', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        limite: '=limite',
        arreglo: '=arreglo',
        inicio: '=inicio',
        busqueda: '=busqueda',
        cambio: '=cambio'
      },
      templateUrl:"views/directivas/jsPaginacion.html",
      controller: "JSPaginacionController",
      // controller: function($element, $transclude) {
      //   $element.find('#pagContent').append($transclude());
      // },
      replace: false
    };
});


appMain.directive('ngFileModel', ['$parse', function ($parse) {
  return {
      restrict: 'A',
      link: function (scope, element, attrs) {
          var model = $parse(attrs.ngFileModel);
          var isMultiple = attrs.multiple;
          var modelSetter = model.assign;
          element.bind('change', function () {
              var values = [];
              angular.forEach(element[0].files, function (item) {
                  var value = {
                     // File Name 
                      name: item.name,
                      //File Size 
                      size: item.size,
                      //File URL to view 
                      url: URL.createObjectURL(item),
                      // File Input Value 
                      _file: item
                  };
                  values.push(value);
              });
              scope.$apply(function () {
                  if (isMultiple) {
                      modelSetter(scope, values);
                  } else {
                      modelSetter(scope, values[0]);
                  }
              });
          });
      }
    };
}]);