<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class ReportesController extends AppController {
	public $uses= array('Reporte', 'Cuadrilla', 'Ciudadano');

	public function getReportes() {
		$this->autoRender=false;

		$reportes = $this->Reporte->getAll();

		$this->JarabeResponse->estado(0);
		$this->JarabeResponse->toast('No se encontraron reportes.');

		if($reportes){
		$this->JarabeResponse->estado(1);
		$this->JarabeResponse->datos('reportes', $reportes);
		$this->JarabeResponse->toast('Información cargada correctamente');
		}
		return $this->JarabeResponse->send();
	}

	public function saveReporte() {
		$this->autoRender = false;

    $ciudadano_id = $this->request->data['ciudadano_id'];
		$calle_principal = $this->request->data['calle_principal'];
		$cruce_uno = $this->request->data['cruce_uno'];
    $cruce_dos = $this->request->data['cruce_dos'];
    $colonia = $this->request->data['colonia'];
    $tipo_suelo = $this->request->data['tipo_suelo'];
    $descripcion = $this->request->data['descripcion'];

		$reporte = $this->Reporte->saveOne($ciudadano_id, $calle_principal, $cruce_uno, $cruce_dos, $colonia, $tipo_suelo, $descripcion);
		$reporte_id = intval($reporte['Reporte']['id']);
		$this->JarabeResponse->toast('Error al guardar');

		if ($reporte_id) {
      $this->JarabeResponse->estado(1);
			$this->JarabeResponse->datos('data', $reporte);
			$this->JarabeResponse->toast("Reporte guardado correctamente!");
			}
		return $this->JarabeResponse->send();
	}

}
