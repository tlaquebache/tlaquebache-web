<?php
/**
* Application level Controller
*
* This file is application-wide controller file. You can put all
* application-wide controller-related methods here.
*
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.Controller
* @since         CakePHP(tm) v 0.2.9
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

/**
* Application Controller
*
* Add your application-wide methods in the class below, your controllers
* will inherit them.
*
* @package    app.Controller
* @link    http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
*/
class AuditorsController extends AppController {
 public $uses= array('Estado', 'Auditor', 'User','Auto', 'Poste', 'UserToken');
 public $components = array('Mail');

 public function getAuditors () {
 $this->autoRender=false;

 $auditors = $this->Auditor->getAll();

 $this->JarabeResponse->estado(0);
 $this->JarabeResponse->toast('Problema al cargar información.');

 if($auditors){
   $this->JarabeResponse->estado(1);
   $this->JarabeResponse->datos('auditors', $auditors);
   $this->JarabeResponse->toast('Información cargada correctamente');
 }

 return $this->JarabeResponse->send();
 }

 public function getAuditor () {
  $this->autoRender=false;

  $id = $this->request->data['id'];

  $auditorData = $this->Auditor->getOne($id);

  $this->JarabeResponse->estado(0);
  $this->JarabeResponse->toast('Problema al cargar los datos.');

  if($auditorData){
    if(isset($this->request->data['week_start']) && isset($this->request->data['week_end'])){
      $week_start = $this->request->data['week_start'];
      $week_end = $this->request->data['week_end'];

      $postes = $this->Poste->getByWeek($id, $week_start, $week_end);
      if($postes) {
        $auditor=array(
          "auditor"=>$auditorData,
          "postes"=>$postes,
        );
      } else {
        $auditor=array(
          "auditor"=>$auditorData,
          "postes"=> 0,
        );
      }
    } else
      $auditor = $auditorData;

      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->datos('auditor', $auditor);
      $this->JarabeResponse->toast('Auditor cargado correctamente');
    }

  return $this->JarabeResponse->send();
  }

  public function getAuditorsByState () {
   $this->autoRender=false;

   $estado_id = $this->request->data['estado_id'];
   $auditors = $this->Auditor->getByState($estado_id);

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast('Problema al cargar información.');

   if($auditors){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('auditors', $auditors);
     $this->JarabeResponse->toast('Información cargada correctamente');
   }

   return $this->JarabeResponse->send();
  }

 public function addAuditorProject () {
  $this->autoRender=false;

  $id = $this->request->data['auditor_id'];
  $project_id = $this->request->data['project_id'];

  $data = $this->Auditor->addProject($id, $project_id);
  $auditorData = $this->Auditor->getOne($id);

  $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast("Error al agregar el Auditor");
   if($data){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('data', $auditorData);
     $this->JarabeResponse->toast("Auditor agregado correctamente");
   }
   return $this->JarabeResponse->send();
 }
  public function removeAuditorProject () {
  $this->autoRender=false;

  $id = $this->request->data['auditor_id'];
  $project_id = $this->request->data['project_id'];

  $data = $this->Auditor->removeProject($id, $project_id);

  $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast("Error al remover el Auditor");
   if($data){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('data', $data);
     $this->JarabeResponse->toast("Auditor removido correctamente");
   }
   return $this->JarabeResponse->send();
 }


 public function saveAuditor(){
   $this->autoRender=false;

   $nombre_completo = $this->request->data['name'];
   $correo = $this->request->data['email'];
   $telefono = $this->request->data['phone'];
   $direccion = $this->request->data['address'];
   $estado_id = $this->request->data['auditorState'];
   $edad = $this->request->data['age'];
   $contacto_emergencia = $this->request->data['emergContact'];
   $tel_emergencia = $this->request->data['emergPhone'];
   $derecho_auto = $this->request->data['auto_right'];
   $password = $this->User->rand_passwd();
   $code_password = sha1($password);

   $data = "";

   $user_data = array(
     'username' => $correo,
     'password' => $code_password,
     'email' => $correo,
     'type' => 2 );

   $validate_auditor = $this->Auditor->validateAuditor($correo);

   if($validate_auditor == null) {
     $user_data = $this->User->createUser($user_data);
     $user_id = $user_data['User']['id'];
     $tipo_usuario = "auditor";

     $auditorData = $this->Auditor->saveAuditor($user_id, $nombre_completo, $correo, $telefono, $direccion, $edad, $contacto_emergencia, $tel_emergencia, $derecho_auto, $estado_id);

     $auditor_id = $auditorData['Auditor']['id'];
     $data =  $this->Auditor->getOne($auditor_id);

     $this->JarabeResponse->toast("Error al guardar el Auditor");
  }

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast("El correo ya fue registrado anteriormente. ");

   if($data){
    $email_status = $this->Mail->send_mail($correo, $nombre_completo, $password, $tipo_usuario);

    if($email_status) {
      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->datos('data', $data);
      $this->JarabeResponse->toast("Auditor guardado correctamente");
    }
   }

   return $this->JarabeResponse->send();
 }

 public function editAuditor(){
    $this->autoRender=false;

    $id = "";
    $nombre_completo= "";
    $correo = "";
    $telefono = "";
    $direccion = "";
    $estado_id = "";
    $edad = "";
    $contacto_emergencia= "";
    $tel_emergencia = "";
    $derecho_auto = 0;

    $id = $this->request->data['id'];
    $nombre_completo = $this->request->data['nombre_completo'];
    $correo = $this->request->data['correo'];
    $telefono = $this->request->data['telefono'];
    $direccion = $this->request->data['direccion'];
    $estado_id = $this->request->data['estado_id'];
    $edad = $this->request->data['edad'];
    $contacto_emergencia = $this->request->data['contacto_emergencia'];
    $tel_emergencia = $this->request->data['tel_emergencia'];
    $derecho_auto = $this->request->data['derecho_auto'];

      $this->JarabeResponse->estado(0);
    $this->JarabeResponse->toast("Error al editar el auditor");

      if($nombre_completo == "" || $correo == "" || $telefono == "" || $direccion == ""  || $estado_id == "" || $edad == "" || $contacto_emergencia == "" || $tel_emergencia == "" ) {

      $this->JarabeResponse->toast("Faltan datos.");
      return $this->JarabeResponse->send();
      }

      $auditorData = $this->Auditor->editAuditor($id, $nombre_completo, $correo, $telefono, $direccion, $estado_id, $edad, $contacto_emergencia, $tel_emergencia, $derecho_auto);

    if($auditorData == true){

      $data = $this->Auditor->getOne($id);

      $this->JarabeResponse->estado(1);
      $this->JarabeResponse->datos('data', $data);
      $this->JarabeResponse->toast("Auditor editado correctamente");
    }

    return $this->JarabeResponse->send();
   }

   public function saveToken () {
   $this->autoRender=false;

   $id = $this->request->data['id'];
   $token = $this->request->data['token'];

   $userToken = $this->UserToken->saveToken($id, $token);

   $this->JarabeResponse->estado(0);
   $this->JarabeResponse->toast('Problema al guardar el Token.');

   if($userToken){
     $this->JarabeResponse->estado(1);
     $this->JarabeResponse->datos('userToken', $userToken);
     $this->JarabeResponse->toast('Token guardado correctamente');
   }

   return $this->JarabeResponse->send();
  }
}
